<?php 
include 'db.php';
require_once 'header.php';

?>
<!-- Page Content -->
        <div class="container" >
            

            <div class="row" style="">

                <div class="col-md-3" style="margin-top: 100px !important">
                    <p class="lead"><a href="<?php echo $siteParentDir;?>/index.php" >Giftexperienceday.com</a></p>

                </div>


                <div class="col-md-12">

                    <div class="row carousel-holder" style="margin-top:54px !important">

                        <div class="col-md-12">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img class="slide-image" src="<?php echo $siteParentDir;?>/1.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="slide-image" src="<?php echo $siteParentDir;?>/2.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="slide-image" src="<?php echo $siteParentDir;?>/3.jpg" alt="">
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>

                    </div>


                   

                </div>


            </div>

        </div>

    </div>
    <!-- /.container -->
<?php                            require_once 'footer.php';
?>
