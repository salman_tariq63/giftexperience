<?php

define('API', 'PS');
require_once('constants.inc.php');
require_once('classes/class.ClientFactory.php');

//var_dump($_POST);

if (isset($_POST['submit'])) {

    
    
    $oClient = ClientFactory::getClient();

    $oRefineByMerchant = new stdClass();
    $oRefineByMerchant->iId = 3;
    $oRefineByMerchant->sName = 'Merchant';

    $oRefineByCategory = new stdClass();
    $oRefineByCategory->iId = 4;
    $oRefineByCategory->sName = 'Category';

    $oRefineByBrand = new stdClass();
    $oRefineByBrand->iId = 2;
    $oRefineByBrand->sName = 'Brand';

    // Refine by merchants 3
    if (!empty($_POST['merchants'])) {
        foreach ($_POST['merchants'] as $merchant_id) {
            $oRefineByDefinition = new stdClass();
            $oRefineByDefinition->sId = $merchant_id;
            $oRefineByDefinition->sName = '';
            $oRefineByMerchant->oRefineByDefinition[] = $oRefineByDefinition;
        }
    }

    // Refine by categories 4
    if (!empty($_POST['categories'])) {
        $categories= $_POST['categories'];
        
        foreach ( $categories as $category_id) {
            $oRefineByDefinition = new stdClass();
            $oRefineByDefinition->sId = $category_id; // Category ID
            $oRefineByDefinition->sName = '';
            $oRefineByCategory->oRefineByDefinition[] = $oRefineByDefinition;
        }
    }

    // Refine by brands 2
    if (!empty($_POST['brands'])) {
        foreach ($_POST['brands'] as $brand_id) {
            $data=  explode('-', $brand_id);
            $id=$data[0];
            $name=$data[1];
            $oRefineByDefinition = new stdClass();
            $oRefineByDefinition->sId = $id; 
            $oRefineByDefinition->sName = $name;
            $oRefineByBrand->oRefineByDefinition[] = $oRefineByDefinition;
        }
    }

    $aParams = array("sQuery" => $_POST['term'],
        "bAdult" => false,
        "iLimit" => 100,
        "sColumnToReturn"	=>	array("sBrand","sDescription", "sMerchantThumbUrl", "sAwThumbUrl", "sAwImageUrl", "iCategoryId"),
        "oActiveRefineByGroup" => array($oRefineByMerchant, $oRefineByBrand, $oRefineByCategory));

    $oResponse = $oClient->call('getProductList', $aParams);

//    echo '<pre>';
    $sOutput = '';
    $sOutput.= $oClient->__getLastRequest();
    $sOutput.= $oClient->__getLastResponse();

    $sOutput = str_replace('><', ">\n<", $sOutput);

}

print $sOutput;
//print_r($oResponse);
//echo '</pre>';

?>