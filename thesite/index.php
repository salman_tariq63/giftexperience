<?php $siteParentDir="newsite"; 
include 'db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
    <link rel="shortcut icon" href="img/favicon.png" />
    <title>Gift Experience</title>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="css/fontawsome/font-awesome.min.css" />
    <!-- Owl Carousel Assets -->
    <link type="text/css" href="css/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link type="text/css" href="css/owl-carousel/owl.theme.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/main.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
/*        img {
    opacity: 0.5;
    filter: alpha(opacity=50);  For IE8 and earlier 
}

img:hover {
    opacity: 1.0;
    filter: alpha(opacity=100);  For IE8 and earlier 
}*/
    </style>
</head>
<body>

    <header>
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <a href="index.php" class="logo">
                            <img src="img/giftexp.png" class="img-responsive" alt="">
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <form method="get" action="../thesite/search.php">
                        <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" name="term" class="search-query form-control" placeholder="Search" />
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navigation -->
        <nav class="main-nav navbar navbar-default" role="navigation">
            <div class="row-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                    <a class="navbar-brand" href="index.html">Business Casual</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse padding-0 navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="nav-black">
                        <div class="container menu-position">
                            <ul class="nav navbar-nav">
                                <?php
                    $menu = "select * from parent_cats where parent_id=0 and id between 0 and 5";
                    $rsmen = mysqli_query($conn, $menu);
                    while ($rwmen = mysqli_fetch_assoc($rsmen)) {
                        ?>
                                <li>
                                    <a href="<?php echo '../thesite/';?><?php echo $rwmen['slug'];?>/"><?php echo $rwmen['name']; ?></a>
                                    <ul class="dropdown-menu">
                                        
                                      <?php
                                        $drp = "select * from parent_cats where parent_id=" . $rwmen['id']." order by name asc";
                                        
                                        $rsdrp = mysqli_query($conn, $drp);
                                        ?>
                                        <?php while ($rwdrp = mysqli_fetch_assoc($rsdrp)) { ?>
                                        <li><a  href="<?php echo '../thesite/';?><?php echo $rwdrp['slug'] ?>/"  ><?php echo $rwdrp['name']; ?></a></li>
                                                <?php } ?>
                                    </ul>
                                </li>
                    <?php }?>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="nav-red">
                       <div class="container menu-position">
                            <ul class="nav navbar-nav">
                                <?php
                    $menu = "select * from parent_cats where parent_id=0 and id between 6 and 11";
                    $rsmen = mysqli_query($conn, $menu);
                    while ($rwmen = mysqli_fetch_assoc($rsmen)) {
                        ?>
                                <li>
                                    <a href="<?php echo '../thesite/';?><?php echo $rwmen['slug'];?>/"><?php echo $rwmen['name']; ?></a>
                                    <ul class="dropdown-menu">
                                        
                                      <?php
                                        $drp = "select * from parent_cats where parent_id=" . $rwmen['id']." order by name asc";
                                        
                                        $rsdrp = mysqli_query($conn, $drp);
                                        ?>
                                        <?php while ($rwdrp = mysqli_fetch_assoc($rsdrp)) { ?>
                                        <li><a  href="<?php echo '../thesite/';?><?php echo $rwdrp['slug'] ?>/"  ><?php echo $rwdrp['name']; ?></a></li>
                                                <?php } ?>
                                    </ul>
                                </li>
                    <?php }?>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="nav-black">
                        <div class="container menu-position">
                            <ul class="nav navbar-nav">
                                <?php
                    $menu = "select * from parent_cats where parent_id=0 and id between 12 and 17";
                    $rsmen = mysqli_query($conn, $menu);
                    while ($rwmen = mysqli_fetch_assoc($rsmen)) {
                        ?>
                                <li>
                                    <a href="<?php echo '../thesite/';?><?php echo $rwmen['slug'];?>/"><?php echo $rwmen['name']; ?></a>
                                    <ul class="dropdown-menu">
                                        
                                      <?php
                                        $drp = "select * from parent_cats where parent_id=" . $rwmen['id']." order by name asc";
                                        
                                        $rsdrp = mysqli_query($conn, $drp);
                                        ?>
                                        <?php while ($rwdrp = mysqli_fetch_assoc($rsdrp)) { ?>
                                        <li><a  href="<?php echo '../thesite/';?><?php echo $rwdrp['slug'] ?>/"  ><?php echo $rwdrp['name']; ?></a></li>
                                                <?php } ?>
                                    </ul>
                                </li>
                    <?php }?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- /.navbar-collapse -->
        </nav>
    </header>

    <!-- new slider starts here -->
        <!-- Carousel -->
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?php 
                    $qry="select * from images";
                    $rs=  mysqli_query($conn, $qry);
                    while($rowimg=  mysqli_fetch_assoc($rs)){
                    ?>
                    <div class="item active">
                        <img src="../thesite/acp/carousel/images/<?php echo $rowimg['image'];?>" alt="First slide">
                        <!-- Static Header -->
                        <div class="header-text hidden-xs">
                            <div class="slider-box">
                                <h1>
                                    <span><?php echo $rowimg['title'];?></span>
                                </h1>
                                <br>
                                <h3>
                                    <span>Experience 2017.</span>
                                </h3>
                                <br>
                                <div class="">
                                    <a class="btn btn-theme btn-sm btn-min-block" href="<?php echo "../thesite/".strtolower($rowimg['title'])."/";?>">EXPLORE NOW</a>

                                </div>
                            </div>
                        </div><!-- /header-text -->
                    </div>
                    <?php }?>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div><!-- /carousel -->


    <section class="white category-area">
        <div class="container">
            <div class="section-title">
                BESTSELLER GIFT
            </div>
            <div class="row">
                <div id="category_item" class="owl-carousel">
                    
                    <?php $cat="select * from parent_cats where displayhome=1";
                    $rscat=mysqli_query($conn,$cat);
                    $i=0;
                    while($rowcat=  mysqli_fetch_assoc($rscat)){
                        if($i%2==0){
                           echo "<div class='category-col'>";
                        }
                       ?>
                    
                        <div class="category-item">
                            <div class="category-item-img">
                                <img src="../thesite/uploads/<?php echo $rowcat['image']; ?>" class="img-responsive cat-thumb" alt="">
                            </div>
                            <a href="#" class="cat-title"><?php echo $rowcat['name'];?></a>
                        </div>
                        <?php  
                        if(($i+1)%2==0){
                            echo "</div>";
                        }
                        $i++;
                        ?>
                   

                    <?php }?>
                     </div>
                </div>
            </div>
        </div>
    </section>
    <section class="red">
        <div class="container">
            <div class="section-title">
                BESTSELLER GIFT
            </div>
            <div class="row">
                <div id="product_item" class="owl-carousel">
                    <?php $best="select * from products left join product_class on products.aw_product_id=product_class.product_id where product_class.top=1";
                    $rsb=  mysqli_query($conn, $best);
                    while($rowb=  mysqli_fetch_assoc($rsb)){
                    ?>
                    <div class="product-item">
                        <div class="produc-item-img">
                            <a href="../thesite/details.php?id=<?php echo $rowb['merchant_product_id']; ?>">
                    <img src="<?php echo $rowb['aw_image_url'];?>" class="img-responsive" alt="">
                            </a>
                            <div class="cat-title"><?php echo $rowb['merchant_category'];?></div>
                        </div>
                        <div class="produc-item-dec" >
                            <div style="height:46px !important">
                            <a href="../thesite/details.php?id=<?php echo $rowb['merchant_product_id']; ?>" class="product-title" >
                                <?php echo $rowb['product_name'];?>
                            </a>
                                </div>
                            <div class="product-shortdec">
                                <?php echo substr($rowb['description'],0,100);?>
                            </div>
<!--                            <div class="reviws">
                                <div class="starrr ratable">
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-stary"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span>
                                </div>
                                <span>12 Reviews</span>
                            </div>-->
                        </div>

                    </div>
                    <?php }?> 
                </div> 
                <a href="#" class="view-more black">View More</a>
            </div>
        </div>
    </section>
    <section class="black">
        <div class="container">
            <div class="panel">
                <div class="panel-heading">
                    <!-- Tabs -->
                    <ul class="product_tabs panel-tabs">
                        <li class="active"><a href="#new_arrivals" data-toggle="tab">New</a></li>
                        <li><a href="#top_rated" data-toggle="tab">TOP RATED</a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content tab-slider">
                        <div class="tab-pane active" id="new_arrivals">
                            <div class="row">
                                <div id="new_product_item" class="owl-carousel">
                                    <?php $best="select * from products left join product_class on products.aw_product_id=product_class.product_id where product_class.new=1";
                    $rsb=  mysqli_query($conn, $best);
                    while($rowb=  mysqli_fetch_assoc($rsb)){
                    ?>
                    <div class="product-item">
                        <div class="produc-item-img">
                            <a href="../thesite/details.php?id=<?php echo $rowb['merchant_product_id']; ?>">
                    <img src="<?php echo $rowb['aw_image_url'];?>" class="img-responsive" alt="">
                            </a>
                            <div class="cat-title"><?php echo $rowb['merchant_category'];?></div>
                        </div>
                        <div class="produc-item-dec" >
                            <div style="height:46px !important">
                            <a href="../thesite/details.php?id=<?php echo $rowb['merchant_product_id']; ?>" class="product-title" >
                                <?php echo $rowb['product_name'];?>
                            </a>
                                </div>
                            <div class="product-shortdec">
                                <?php echo substr($rowb['description'],0,100);?>
                            </div>
<!--                            <div class="reviws">
                                <div class="starrr ratable">
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-stary"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span>
                                </div>
                                <span>12 Reviews</span>
                            </div>-->
                        </div>

                    </div>
                    <?php }?>
                                </div>
                                <a href="#" class="view-more red">View More</a>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="top_rated">
                            <div class="row">
                                <div id="top_rated_products" class="owl-carousel">
                                    <?php $best="select * from products left join product_class on products.aw_product_id=product_class.product_id where product_class.top=1";
                    $rsb=  mysqli_query($conn, $best);
                    while($rowb=  mysqli_fetch_assoc($rsb)){
                    ?>
                    <div class="product-item">
                        <div class="produc-item-img">
                            <a href="../thesite/details.php?id=<?php echo $rowb['merchant_product_id']; ?>">
                    <img src="<?php echo $rowb['aw_image_url'];?>" class="img-responsive" alt="">
                            </a>
                            <div class="cat-title"><?php echo $rowb['merchant_category'];?></div>
                        </div>
                        <div class="produc-item-dec" >
                            <div style="height:46px !important">
                            <a href="../thesite/details.php?id=<?php echo $rowb['merchant_product_id']; ?>" class="product-title" >
                                <?php echo $rowb['product_name'];?>
                            </a>
                                </div>
                            <div class="product-shortdec">
                                <?php echo substr($rowb['description'],0,100);?>
                            </div>
<!--                            <div class="reviws">
                                <div class="starrr ratable">
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-stary"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star"></span>
                                    <span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span>
                                </div>
                                <span>12 Reviews</span>
                            </div>-->
                        </div>

                    </div>
                    <?php }?>
                                    
                                </div>
                                <a href="#" class="view-more red">View More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h3>NEED HELP?</h3>
                    <p>1 (800) 541-4441  <span class="font-red"> (7am - 5pm PST)</span></p>
                    <ul>
                        <li><a href="#">About giftexperienceday.com</a> </li>
                        <li><a href="#">Help Center</a> </li>
                        <li><a href="#">How it works</a> </li>
                        <li><a href="#">Gift Certificates</a> </li>
                        <li><a href="#">Membership</a> </li>
                        <li><a href="#">F.A.Q</a> </li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h3>Categories</h3>
                    <ul>
                        <li><a href="#">Adventure</a> </li>
                        <li><a href="#">Animals</a> </li>
                        <li><a href="#">Beauty & Pampering</a> </li>
                        <li><a href="#">Days Out</a> </li>
                        <li><a href="#">Driving</a> </li>
                        <li><a href="#">Flying</a> </li>
                        <li><a href="#">Food & Drink</a> </li>
                        <li><a href="#">Hobbies & Activities</a> </li>
                        <li><a href="#">London</a> </li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <ul>
                        <li><a href="#">Membership</a> </li>
                        <li><a href="#">Short Breaks</a> </li>
                        <li><a href="#">Sightseeing & Attractions</a> </li>
                        <li><a href="#">Spa Days</a> </li>
                        <li><a href="#">Sports</a> </li>
                        <li><a href="#">Theme Parks</a> </li>
                        <li><a href="#">Tours</a> </li>
                        <li><a href="#">Trains</a> </li>
                        <li><a href="#">Water</a> </li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h3>SAY HELLO!</h3>
                    <p>California, US, Lorem Lis Street, Orange, 25</p>
                    <p>Phone: 800 122 3456</p>
                    <p>Fax: 800 122 3456</p>
                    <p>Email: <span class="font-red">info@giftexperienceday.com</span></p>
                    <h3>Social Media</h3>
                    <ul class="social">
                        <li><a href="#" class="facebook"> </a> </li>
                        <li><a href="#" class="twitter"> </a> </li>
                        <li><a href="#" class="instagram"> </a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <div class="copy-right black text-center">
        <div class="container">
            <p>Copyright © Giftexperience 2017.</p>
        </div>
    </div>


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!--Owl Caurosel-->
    <script type="text/javascript" src="js/owl-carousel/owl.carousel.js"></script>

    <script type="text/javascript">
        $("#category_item").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 200,
            paginationSpeed: 800,
            pagination: false,
            rewindSpeed: 1000,
            items: 4
        });
        $("#product_item").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 200,
            paginationSpeed: 800,
            pagination: false,
            rewindSpeed: 1000,
            items: 4
        });
        $("#product_item1").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 200,
            paginationSpeed: 800,
            pagination: false,
            rewindSpeed: 1000,
            items: 4
        });
        $("#product_item2").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 200,
            paginationSpeed: 800,
            pagination: false,
            rewindSpeed: 1000,
            items: 4
        });
        $("#new_product_item").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 200,
            paginationSpeed: 800,
            pagination: false,
            rewindSpeed: 1000,
            items: 4
        });
        $("#top_rated_products").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 200,
            paginationSpeed: 800,
            pagination: false,
            rewindSpeed: 1000,
            items: 4
        });
    </script>

    <!--<script type="text/javascript" src="js/main.js"></script>-->
</body>
</html>
