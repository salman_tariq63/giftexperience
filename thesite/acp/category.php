<?php
require_once 'header.php';

if (!empty($_POST['go'])) {
    if (!empty($_POST['id'])) {

        foreach ($_POST['id'] as $categoryId) {
            DB::delete('parent_cats', "id=%d", $categoryId);
            $messages[] = "Category Deleted!";
        }
    }
}
if (!empty($_POST['modified'])) {
   if($_POST['linesmodified']){
       $ids=  explode(",",$_POST['linesmodified']);
       foreach($ids as $id){
           if($id!="0"){
               if(isset($_POST['display'][$id])){
                   $display=$_POST['display'][$id];
               }else{
                   $display='0';
               }
               $allow = array("jpg", "jpeg", "gif", "png");

$todir = '../uploads/';

 // is the file uploaded yet?
//print_r($_FILES);
foreach($_FILES["picture"]['name'] as $pic){
    
       
        foreach ($_FILES["picture"]["error"] as $key => $error) {
    if ($error == UPLOAD_ERR_OK) {
        $info = explode('.', strtolower($_FILES["picture"]["name"][$key]) );
        $tmp_name = $_FILES["picture"]["tmp_name"][$key];
        // basename() may prevent filesystem traversal attacks;
        // further validation/sanitation of the filename may be appropriate
        $name = basename($_FILES["picture"]["name"][$key]);
        if ( in_array( end($info), $allow) ){
        move_uploaded_file($tmp_name, "$todir/$name");
     
        }else{
            echo "The Picture Format Is Not Allowed";
        }
    }
}
    
    

    }
    if($_FILES["picture"]["name"][$id]!==""){
    $image_name=$_FILES["picture"]["name"][$id];}else{
     $image_name=$_POST['image'][$id];   
    }
           $tag=$_POST['tags'][$id];
           $slug=$_POST['slug'][$id];
           $position=$_POST['position'][$id];
            DB::update('parent_cats', array(
  'tags' => "$tag",
   'slug' => "$slug",
   'displayhome'=>"$display",
    'image'=>"$image_name",
   'position'=>"$position"
  ), "id=%d", "$id");
           
           }
           

          
           
       }
       
   }
   


}
$order = "asc";

if (!empty($_GET['order'])) {
    if ($_GET['order'] == 1) {
        $order = "desc";
    }
}



if(!empty($_POST['search'])){
   $category_id=$_POST['category'];
   $catname=$_POST['catname'];
$where = new WhereClause('and');
if(!empty($_POST['catname'])){
$where->add('name LIKE %ss', "$catname");}
if(!empty($_POST['category'])){
$where->add('parent_id=%i', "$category_id");}
$category = DB::query("SELECT * FROM parent_cats WHERE %l", $where); 


//print_r($category);
}else{
     
         $rec_limit = 50;
         
         if( isset($_GET['page']) ) {
            $page = $_GET['page'] + 1;
            $offset = $rec_limit * $page ;
         }else {
            $page = 0;
            $offset = 0;
         }
         
        
  $category = DB::query("select * from parent_cats order by name $order LIMIT $offset, $rec_limit"); 
  $rec_count=DB::count();
   $left_rec = $rec_count - ($page * $rec_limit);
  $tpages = ($rec_count) ? ceil($rec_count / 50) : 1;
}
if(!empty($_GET['catname'])or!empty($_GET['category'])){
   $category_id=$_GET['category'];
   $catname=$_GET['catname'];
$where = new WhereClause('and');
if(!empty($_GET['catname'])){
$where->add('name LIKE %ss', $catname);}
if(!empty($_GET['category'])){
$where->add('parent_id=%i', "$category_id");}
$category = DB::query("SELECT * FROM parent_cats WHERE %l order by name $order", $where);  
    
}

?>

<div class="jumbotron">
    <h3>Categories</h3>
    <a href="categoryadd.php">Add new</a><br><br>
    <form method="post" class="form-horizontal" enctype="multipart/form-data">
        <input type='hidden' name='linesmodified' value="0" id="modified"/>
        <select name="category" class="form-control">
            <?php if(!empty($_POST['category'])){
            $selected=DB::queryRaw("SELECT * FROM parent_cats WHERE id=%s", $_POST['category']);
            foreach($selected as $select){?>
            <option value="<?php echo $select['id']; ?>"><?php echo $select['name']; ?></option>
            <?php }}else{?>
            <option value="">Select parent category</option>
            <?php } $drocat = DB::query("select * from parent_cats where parent_id=0 order by name $order"); 
           foreach($drocat as $option){
           ?>
            <option value="<?php echo $option['id']; ?>"><?php echo $option['name']; ?></option>
           <?php }?>
        </select><br>
        <input type="text" name="catname" class="form-control" value="<?php if(!empty($_POST['catname'])){echo $_POST['catname']; }?>" placeholder="Enter the category you want to search"/><br>
        <button type="submit" name="search" value="search" class="btn btn-default btn-xs">Search</button>
        <table class="table table-striped" width="100%">
            <thead>
                <tr align="right">
                    <td colspan="10">

                        <button type="submit" name="go" value="Go" class="btn btn-default btn-xs">Delete</button>
                        <button type="submit" name="modified" value="modified" class="btn btn-default btn-xs">Modify</button>
                    </td>
                </tr>
                <tr>
                    <th></th> 
                    <th>Parent Name</th>
                    <th>
                        Name <a href="category.php?order=<?php echo isset($_GET['order']) ? !$_GET['order'] : 1; ?>&catname=<?php if(!empty($_POST['catname'])){echo $_POST['catname'];}if(!empty($_GET['catname'])){echo $_GET['catname'];} ?>&category=<?php if(!empty($_POST['category'])){echo $_POST['category'];}if(!empty($_GET['category'])){echo $_GET['category'];} ?>"><i class="fa fa-fw fa-sort"></i></a>
                    </th>
                    <th>Display On Home</th>
                    <th>Upload Image</th>
                    <th>Position</th>
                    <th></th>
                    <th>Tags</th>
                    <th>Slug</th>
                    <th>Edit</th>


                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($category as $clone) {
                    ?>
            <input type="hidden" name="image[<?php echo $clone['id'];?>]" value="<?php echo $clone['image'] ?>"/>
                  
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?= $clone['id'] ?>"></td>
                        
                        <td>
                            <?php
                            $parent = DB::query("select name from parent_cats where id=".$clone['parent_id']);
                            foreach($parent as $p){
                                echo $p['name'];
                            }
                            ?>
                        </td>
                        <td><a href='/thesite/<?php echo $clone['name'];  ?>/' target="_blank"><?= $clone['name']; ?></a></td>
                        <td><input  <?php if($clone['displayhome']==1){ echo "checked='checked'";} ?> type="checkbox" name="display[<?php echo $clone['id'];?>]" value="1" onchange="textchange(<?php echo $clone['id']; ?>);" /></td>
                        <td><input type="file" name="picture[<?php echo $clone['id'];?>]" value="<?php echo $clone['image'];?>" id="file" onchange="textchange(<?php echo $clone['id']; ?>);"><img id="blah" height="100px" width="100px" src="<?php echo "../uploads/".$clone['image'];?>" alt="your image" /></td>
                        <td><input class="form-control" type="text" name="position[<?php echo $clone['id'];?>]" value='<?php echo $clone['position'] ?>' onchange="textchange(<?php echo $clone['id']; ?>);" onkeyup="textchange(<?php echo $clone['id']; ?>);"/></td>
                        <td><a href="results.php?term=<?php echo $clone['slug'];?>&catid=<?php echo $clone['id'];?>" target="_blank">Customise results</a></td>
                        <td><input class="form-control" type="text" name="tags[<?php echo $clone['id'];?>]" value='<?php echo $clone['tags'] ?>' onchange="textchange(<?php echo $clone['id']; ?>);" onkeyup="textchange(<?php echo $clone['id']; ?>);"/></td>
                        <td><input class="form-control" type="text" name="slug[<?php echo $clone['id'];?>]" value='<?php echo $clone['slug'] ?>' onchange="textchange(<?php echo $clone['id']; ?>);" onkeyup="textchange(<?php echo $clone['id']; ?>);" /></td>
                        <td>
                            <a href="categoryadd.php?id=<?= $clone['id'] ?>">
                                Edit</a></td>

                    </tr>
                    <?php
                }
                ?>       
            </tbody>
        </table>

        
        
    </form>
    <?php
    
       if(empty($_POST['search'])){
           if(empty($_GET['catname'])){
           if(empty($_GET['category'])) {   
          // print_r($_POST);
//        if( $page > 0 ) {
//            $last = $page - 2;
//            echo "<a href = \"category.php?page=$last\"><b>Previous</b></a> |";
//            echo "<a href = \"category.php?page=$page\"><b>Next</b></a>";
//         }else if( $page == 0 ) {
//            echo "<a href = \"category.php?page=$page\"><b>Next</b></a>";
//         }else if( $left_rec < $rec_limit ) {
//            $last = $page - 2;
//            echo "<a href = \"category.php?page=$last\"><b>Previous</b></a>";
//         }
       
       echo "<ul class=\"setPaginate\">";
       for($i=-1;$i<=15;$i++){
           $j=$i+1;
          echo "<li><a href = \"category.php?page=$i\"><b>$j</b></a></li>"; 
       }
       echo "</ul>";
           }
           }
           
       }
      ?>
   
</div>
</div>

<?php
require 'footer.php';
?>

