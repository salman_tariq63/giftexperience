<?php
require_once 'header.php';

if (!empty($_POST['go'])) {
    if (!empty($_POST['id'])) {
        foreach ($_POST['id'] as $cloneId) {
            DB::delete('clones', "id=%d", $cloneId);
            $messages[] = "Clones Deleted!";
        }
    }
}

$clones = DB::query("select c.*, ct.name as category_name from clones c"
                . " left join category ct on"
                . " c.category_id = ct.id ");

//print_r($clones);
?>

<div class="jumbotron">
    <h3>Clones</h3>
    <a href="clonesadd.php">Add new</a>

    <div>
<?php
//displayErrors($errors);
//displayMessages($messages);
?>
    </div>

    <form method="post" class="form-horizontal">

        <table class="table table-striped" width="100%">
            <thead>
                <tr align="right">
                    <td colspan="10">

                        <button type="submit" name="go" value="Go" class="btn btn-default btn-xs">Delete</button>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Active </th>
                    <th>Overlay</th>
                    <th>Affiliates</th>
                    <th>Cling Url</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
<?php
foreach ($clones as $clone) {
    ?>
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?= $clone['id'] ?>"></td>
                        <td><?= $clone['id'] ?></td>
                        <td><a href='clonesadd.php?id=<?= $clone['id'] ?>'><?= $clone['name'] ?></a></td>
                        <td><?= $clone['category_name'] ?></td>
                        <td><?= $clone['active'] == 1 ? "Yes" : "No" ?></td>
                        <td><?= $clone['overlay'] == 1 ? "Yes" : "No" ?></td>
                        <td><?= $clone['affiliate'] ?></td>
                        <td><?= $clone['cling_url'] ?></td>
                        <td>
                            <a href="clonesadd.php?id=<?= $clone['id'] ?>">
                                Edit</a></td>
                    </tr>
    <?php
}
?>        
            </tbody>
        </table>
    </form>
</div>
</div>

<?
require 'footer.php';
?>
