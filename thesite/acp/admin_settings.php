<?php
require_once 'header.php';

if(!empty($_POST['save'])){
    if(!empty($_POST['newPassword'])){
        if($_POST['newPassword']<>$_POST['retypeNewPassword']){
            $errors[] = "Password and retype password fields did not match";
        }
        else{
        $password = $_POST['newPassword'];
        $email = $_POST['adminEmail'];
        $role = 'editor';
        setSettings($password,$email,$role);
        }     
    }
}

$adminEmail = getSettings("adminEmail");
?>

      <div class="jumbotron">
        <h3>Admin Settings</h3>
        <a href="editor_settings.php" style="float:right; text-decoration:none;"><h4>Change Editor Settings</h4></a>
        <div>
       
        </div>
        
        <form method="post" class="form-horizontal">
            <div class="form-group">
                <h4>Change admin info</h4>
            </div>
            
            <div class="form-group">
                <label>Admin email address</label><input type="email" name="adminEmail" required="" class="form-control" placeholder="Admin Email Address" value="<?=$adminEmail?>">
            </div>
            
            <div class="form-group">
                <label>New password</label><input type="password" name="newPassword" class="form-control" placeholder="New Password">
            </div>
            
            <div class="form-group">
                <label>Retype new password</label><input type="password" name="retypeNewPassword" class="form-control" placeholder="Rettype New Password">
            </div>
                        
            <div class="form-group">
                <button type="submit" name="save" value="save" class="btn btn-default">Save</button>
            </div>
        </form>
        </div>
      </div>

<?
require 'footer.php';
?>
