<html><head>
        <link rel="stylesheet" href="../acp/css/select2.min.css">
        
    </head></html>
<?php
require_once 'header.php';
//$agents = DB::query("select id, name from agents");

// Edit
if (!empty($_GET['id'])) {
    $cloneData = DB::queryFirstRow("select * from parent_cats where id=%d", $_GET['id']);
//    $cloneData['phonenumbers'] = unserialize ($cloneData['phonenumbers']);
    $formAction = "Modify";
} else {
    $formAction = "Save";
}

if (!empty($_POST['Save'])) {

//    if (!empty($_POST['activeAgents'])) {
//        $activeAgents = implode(",", $_POST['activeAgents']);
//    }
//   $slug = formatstr($_POST['name']);
    if(!empty($_POST['joincat'])){
    $joincats=implode(",",$_POST['joincat']);}else{
      $joincats="0";  
    }
$mer=implode(",",$_POST['merchants']);
    DB::insert('parent_cats', array(
        "name" => $_POST['name'],
        "slug" => $_POST['slug'],
        "description" =>$_POST['description'],
        "tags" =>$_POST['tags'],
        "parent_id" =>$_POST['parentid'],
        "merchants"=>$mer,
        "joinedcats"=>$joincats
//        "active" => $_POST['active'],
//        "activeAgents" => $activeAgents
    ));

    header("location:category.php");
}

if (!empty($_POST['Modify'])) {

//    if (!empty($_POST['activeAgents'])) {
//        $activeAgents = implode(",", $_POST['activeAgents']);
//    }
//        $slug_edit = formatstr($_POST['name']);
$merup=implode(",",$_POST['merchants']);
$joincats=implode(",",$_POST['joincat']);
    DB::update('parent_cats', array(
        "name" => $_POST['name'],
        "slug" => $_POST['slug'] ,
        "description" =>$_POST['description'],
        "tags" =>$_POST['tags'],
        "parent_id" =>$_POST['parentid'],
        "merchants"=>$merup,
        "joinedcats"=>$joincats
//        "active" => $_POST['active'],
//        "activeAgents" => $activeAgents
            ), "id=%d", $_POST['id']);

    header("location:category.php");
}
?>

<div class="jumbotron">
    <?php 
    if($formAction == 'Save'){ ?>
    <h3>Add Category</h3>
    <?php } else { ?>
    <h3>Edit Category</h3>
    <?php } ?>
    <div>
        <?php
//        displayErrors($errors);
//        displayMessages($messages);
        ?>
    </div>

    <form method="post" class="form-horizontal">
<div class="form-group">
          <label>Parent Id</label>
          <?php if($formAction == 'Save'){ ?>
          <select name="parentid" id="parentcat" class="form-control">
              <option value="" >Select Parent Category</option>
              <option value="0">root</option>
              <?php 
              $data=DB::query("select id,name from parent_cats");
             // print_r($data);
              foreach($data as $cats){
              ?>
              <option value="<?php echo $cats['id'];?>"><?php echo $cats['name'];?></option>
                  
              
              <?php }?>
          </select><?php }else{?>
          <select name="parentid" id="parentcat" class="form-control" >
              <option value="<?php echo $cloneData['parent_id'];?>"><?php if($cloneData['parent_id']==0){ echo "root";}else{$data2=DB::query("select name from parent_cats where id=".$cloneData['parent_id']); foreach($data2 as $name){echo $name['name'];}}?></option>
              <?php 
              $data=DB::query("select id,name from parent_cats");
              print_r($data);
              foreach($data as $cats){
              ?>
              <option value="<?php echo $cats['id'];?>"><?php echo $cats['name'];?></option>
                  
              
              <?php }?>
          </select>
         
              <?php }?>
        </div>
        <div class="form-group">
          <label>Name</label>
          <?php if($formAction == 'Save'){ ?>
          <input type="text" name="name" class="form-control" placeholder="Enter Category Name" required="" value=""><?php }else{?>
           <input type="text" name="name" class="form-control" placeholder="Enter Category Name" required="" value="<?=$cloneData['name']?>">
              <?php }?>
        </div>
        <div class="form-group">
            <label>Description</label>
               <?php if($formAction == 'Save'){ ?>
               <textarea  placeholder="description" name="description" id="description"  cols="50" rows="10" value=""></textarea> <?php } else {  ?>
               <textarea  placeholder="description" name="description" id="description"  cols="50" rows="10" value="<?=$cloneData['description']?>"></textarea> <?php  }?>
        </div>
        <div class="form-group">
            <label>Tags</label>
            <?php if($formAction == 'Save'){ ?>
            <input type="text" class="form-control" placeholder="Enter comma separated tags" name="tags" id="tags"  value=""><?php }else{?>
            <input type="text" class="form-control" placeholder="Enter comma separated tags" name="tags" id="tags"  value="<?=$cloneData['tags']?>">
                <?php }?>
            </div>
        <div class="form-group">
            <?php if($formAction == 'Save'){ ?>
            <label>Slug</label>
            <input type="text" class="form-control" placeholder="Enter slug" name="slug" id="slug"  value=""><?php }else{?>
            <label>Slug</label>
            <input type="text" class="form-control" placeholder="description" name="slug" id="slug"   value="<?=$cloneData['slug']?>">
                <?php }?>
            </div>
        <div class="form-group">
          <label>Join categories</label>
          <?php if($formAction == 'Save'){ ?>
          <select name="joincat[]" id="joincat" class="form-control" multiple="multiple">
              
              <option value="0">root</option>
              <?php 
              $data=DB::query("select id,name from parent_cats");
             // print_r($data);
              
              foreach($data as $cats){
                  
              ?>
              <option  value="<?php echo $cats['id'];?>"><?php echo $cats['name'];?></option>
                  
              
              <?php }?>
          </select><?php }else{?>
          <select name="joincat[]" id="joincat" class="form-control" multiple="multiple">
              <option value="<?php echo $cloneData['parent_id'];?>"><?php if($cloneData['parent_id']==0){ echo "root";}else{$data2=DB::query("select name from parent_cats where id=".$cloneData['parent_id']); foreach($data2 as $name){echo $name['name'];}}?></option>
              <?php 
              $data=DB::query("select id,name,joinedcats from parent_cats");
              $datajoin=DB::query("select joinedcats from parent_cats where id=".$_GET['id']);
              
              print_r($datajoin);
              foreach($data as $cats){
                  
                  $selected='';
                  
                  foreach($datajoin as $joincat){
                  $join=  explode(",", $joincat['joinedcats']);
                  if(in_array($cats['id'], $join)){
                      $selected="selected='selected'";
                  }
                  }
              ?>
              <option <?php echo $selected; ?> value="<?php echo $cats['id'];?>"><?php echo $cats['name'];?></option>
                  
              
              <?php }?>
          </select>
         
              <?php }?>
        </div>
        <div class="form-group">
            <label>merchants</label><br>
            <?php
            $merchants=DB::query("select distinct merchant_id,merchant_name from products");
            foreach($merchants as $merchant){
                $check="";
                if(!empty($_GET['id'])){
                $merchant_id=DB::query("select merchants from parent_cats where id=".$_GET['id']);
                foreach($merchant_id as $merid){
            if(in_array($merchant['merchant_id'], explode(",",$merid['merchants']))){
                $check="checked='checked'";
            }
                }}
            ?>
            <input <?php echo $check;  ?> type="checkbox" name="merchants[]" value="<?php echo $merchant['merchant_id'];?>"/><?php echo $merchant['merchant_name']; ?><br>
            
            <?php }?>
        </div><br>
        <div class="form-group">
            
            <?php if($formAction !== 'Save'){ ?>
            <input type="hidden" name="id" value="<?= $_GET['id'] ?>"><?php }?>
            <button type="submit" name="<?= $formAction ?>" value="<?= $formAction ?>" class="btn btn-default"><?= $formAction ?></button>
        </div>
        
    </form>

</div>

<script src="js/jquery-1.10.2.js"></script>
    <script src="js/select2.full.min.js"></script>
    <script type="text/javascript">
         $("#parentcat").select2({
                                                placeholder: "Select a parent category"
                                            });
                                            
         $("#joincat").select2({
                                                placeholder: "Select categories to join"
                                            });                                   
    </script>
     <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    
<!--    <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/select2.full.min.js"></script>-->
    <script type="text/javascript">
        tinymce.init({selector:'textarea'});
        
</script>
<?php
require 'footer.php';
?>

