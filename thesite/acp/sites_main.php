
<?php
include "header.php";

if (!empty($_GET['a']) and $_GET['a'] == 'delete') {
    $qry = "select logo from sites where id={$_GET['id']}";
    $rs = db::query($qry);
    @unlink("../{$row['logo']}");

    $qry = "delete  from sites where id={$_GET['id']}";
    $rs = db::queryRaw($qry);
}

// For cateogries , tabs and search bar 
if (!isset($_GET['tab']) && !isset($_GET['category']) && !isset($_GET['search'])) {
    $qry1 = "select * from sites";
    $rs = db::query($qry1);
} else if (isset($_GET['tab'])) {
    $tabid = $_GET['tab'];

    $queryTab = "select s.position,s.id,s.title,s.bonus,s.logo,s.deposit,s.devices,s.popularity,s.commission,sc.tab_id, sc.position from sites as s left join sites_tab as sc on s.id = sc.site_id left join tab as c on sc.tab_id = c.id  where sc.tab_id ='$tabid'";

    $rs = db::query($queryTab);
} else if (isset($_GET['category'])) {
    $catid = $_GET['category'];
    $qry_cat = "select s.position,s.id,s.title,s.bonus,s.logo,s.deposit,s.devices,s.popularity,s.commission,sc.category_id, sc.position from sites as s left join sites_category as sc on s.id = sc.site_id left join category as c on sc.category_id = c.id  where sc.category_id ='$catid'";
    $rs = db::query($qry_cat);
} else if (isset($_GET['search'])) {
    $name = $_GET['search'];

    $qry_name = "select * from sites where title LIKE '%$name%'";

    $rs = db::query($qry_name);
}
?>

<script language="JavaScript">
    function delConfirm(id)
    {
        if (confirm("Are you sure you want to delete?"))
            location.href = "sites_main.php?a=delete&id=" + id
        else
            return
    }
</script>
<script>
    $(document).ready(function ()
    {
        $("#myTable").tablesorter();
    }
    );
</script>

<div class="jumbotron">
    <h3>Site Manager</h3>
    <div class="cat">
        <table class="table table-bordered">
            <td><h5><strong>Category</strong></h5></td>
            <td>
                <div class="inner">
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
                        <select align="right" name="category" onchange="this.form.submit()">
                            <option>Select Category </option>
                            <?php
                            $categories = DB::query("SELECT * FROM category");

                            foreach ($categories as $category) {

                                $category_id = $category['id'];
                                $category_title = $category['name'];
                                if ($_GET['category'] == $category_id) {
                                    echo "<option value='$category_id' selected>$category_title</option>";
                                } else {
                                    echo "<option value='$category_id' >$category_title</option>";
                                }
                            }
                            ?>  
                        </select>
                    </form>
                </div>
            </td>
            <td><h5> <a href="sites_add.php?category=<?= $catid ?>">Add New Site To Category</a></h5></td>
            <td> <h5><a href="exsistingSiteToCategory.php?category=<?= $catid ?>">Add Existing Site To Category</a></h5></td>
        </table>
        <table class="table table-bordered">
            <td><h5><strong>Tabs</strong></h5></td>

            <td>
                <div class="inner">
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">

                        <select align="right" name="tab" onchange="this.form.submit()">
                            <option>Select Tab </option>
                            <?php
                            $tabs = DB::query("SELECT * FROM tab");

                            foreach ($tabs as $tab) {

                                $tab_id = $tab['id'];
                                $tab_title = $tab['name'];
                                if ($_GET['tab'] == $tab_id) {
                                    echo "<option value='$tab_id' selected>$tab_title</option>";
                                } else {
                                    echo "<option value='$tab_id' >$tab_title</option>";
                                }
                            }
                            ?>  
                        </select>
                    </form>
                </div>
            </td>
            <td><h5> <a href="sites_add.php?tab=<?= $tabid ?>">Add New Site To Tab</a></h5></td>
            <td> <h5><a href="exsistingSiteToTab.php?tab=<?= $tabid ?>">Add Existing Site To Tab</a></h5></td>
        </table>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
            <input type="text" name="search" placeholder="Enter Site Name"/> 
            <input type="submit" value="Search" onchange="this.form.submit()" />
        </form>
    </div>
    <h4 align="left"><a href="sites_add.php">Add New Site</a></h4>
    <p>
    <form  method="post"  enctype="multipart/form-data">

        <table  id="myTable"  class="table tablesorter table-striped table-hover table-bordered" >
            <thead>
                <tr>
                    <th align="left">ID</th>
                    <th align="left">Position</th>
                    <th align="left">Logo</th>
                    <th align="left">Title</th>        
                    <th align="left">Bonus</th>
                    <th align="left">Deposit</th>
                    <th align="left">Devices</th>
                    <th align="left">Tabs</th>
                    <th align="center">Edit</th>
                    <?php if ($role == 'admin') { ?>
                        <th align="center">Delete</th>
                    <?php } ?>

                </tr>
            <input id="submit" name="submit" type="submit" class="button" value="Save Position And Tabs" /> 
            </thead>
            <tbody>
                <?php
                foreach ($rs as $row) {
                    ?>      
                    <tr class="row<?= @$i++ % 2 ?>">
                <input type="hidden" name="idd[]" value="<?= $row['id'] ?>">
                <?php
                $currentid = $row['id'];
                $c = DB::queryFirstRow("select position from sites_category where site_id = $currentid ");
                ?>
                 <td align="left"><?= $row['id'] ?></td>
                <input type="hidden" name="hiddenposition" value="<?= $c['position'] ?>">
                
                <td><input type="text" name="position[<?= $row['id'] ?>]" size="2" value="<?= $c['position'] ?>">                                    </td>
                <td align="left">
                    <a href='//<?= $row['url'] ?>/'> <img src="../images/logo/<?php echo $row['logo']; ?>" ></a>
                </td>
                <td align="left"><a href='//<?= $row['url'] ?>/'><?= $row['title'] ?></a></td>
                <td align="left"><?= $row['bonus'] ?></td>
                <td align="left"><?= $row['deposit'] ?></td>

                <td align="left"><?= $row['devices'] ?></td>
                <td>
                    <?php
                    $cc = DB::query("select * from sites_tab where site_id = $currentid ");
                    $u =0;$uu=0;
                    foreach($cc as $ccc){
                        if($ccc['tab_id'] == '2'){
                            $u = 1;
                        }
                        if($ccc['tab_id'] == '3'){
                            $uu = 1;
                        }
                    }
                    ?>
                    <input type="checkbox" name="editor[]" <?=  $u != 1 ? '' : 'checked' ?> value="<?= $row['id'] ?>">  Editors Choice<br/>
                    
                    <input type="checkbox" name="top[]" <?= $uu != 1 ? '' : 'checked' ?> value="<?= $row['id'] ?>">  Top Slots
                </td>
                   
                <td align="center">
                    <a href="sites_edit.php?id=<?= $row['id'] ?>">
                        Edit</a></td>
    <?php if ($role == 'admin') { ?>
                    <td align="center">
                        <a href="javascript:delConfirm('<?= $row['id'] ?>')">
                            Delete</a></td>
    <?php } ?>
                </tr>
                <?php
            }
            ?>     
            </tbody>
        </table>
    </form>
    <p></p>
</div>
<?php
if (isset($_POST['submit'])) {
    if (!empty($_POST['editor']) && !isset($_POST['editor'])) {
        foreach ($_POST['editor'] as $editor_id) {
            DB::insert('sites_tab', array("site_id" => $editor_id, "tab_id" => 2, "position" => $_POST['position']));
        }
    }
    if (!empty($_POST['top'])&& !isset($_POST['top'])) {
        foreach ($_POST['top'] as $top_id) {
            //$sql = "update sites set top_slots = 1 where id = '$top_id' ";
            //db::queryRaw($sql);
            DB::insert('sites_tab', array("site_id" => $editor_id, "tab_id" => 3, "position" => $_POST['position']));
        }
    }
    if ($_POST['hiddenposition'] != $_POST['position'])  {
        foreach ($_POST['idd'] as $id) {
            $i = $_POST['position'][$id];
            $sql = "update sites_category set position = $i where site_id =$id";
            $s = DB::queryRaw($sql);
            $sqll = "update sites_tab set position = $i where site_id =$id";
            $s = DB::queryRaw($sqll);
            $sqll = "update sites_tab set position = $i where site_id =$id";
            $s = DB::queryRaw($sqll);
            $sqlll = "update sites set position = $i where id =$id";
            $s = DB::queryRaw($sqlll);
        }
    }
}
?>
<style>
    .success{
        font-size: 20px;
    }
    table.tablesorter{
        margin-top: 42px;
        margin-left: -49px;
    }
    .cat{
        float:right;
        margin-top :-50px;
        margin-right: 50px;

        padding-top: 18px;
        padding-bottom: 20px;

    }
    .cat h5 a{
        text-decoration: none;
    }
    .inner{
        margin-top:5px;
    }

</style>
<?php
include "footer.php";
?>
