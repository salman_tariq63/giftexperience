
<?php
include "header.php";
if (!empty($_GET['category'])) {
    $catid = $_GET['category'];
}
if (!empty($_POST['submit'])) {
    if (empty($_POST["title"]) and empty($_POST['promotion'])) {
        $error = "Please enter Site title and promotion";
    } else {
        //for logo
        $logo = $_FILES['logo']['name'];
        $temp_name1 = $_FILES['logo']['tmp_name'];
        move_uploaded_file($temp_name1, "../images/logo/$logo");
        
        // for Round Logo
        $roundlogo = $_FILES['roundlogo']['name'];
        $temp_name2 = $_FILES['roundlogo']['tmp_name'];
        move_uploaded_file($temp_name2, "../images/roundlogo/$roundlogo");
        
        
        
        $desc = htmlentities($_POST['description'], ENT_QUOTES, "UTF-8");
        //$firstname = mysqli_real_escape_string($con, $_POST['firstname']);

        $slug = formatstr($_POST['title']);
        //echo $slug;
        //exit();
        $checkBoxDevices = @implode(',', $_POST['devices']);
        
        //DB::insert('admin_sessions', array("sessionId" => session_id(), "active" => 1, "role" => $role));
        DB::insert('sites', array("logo" => $logo,"roundlogo" => $roundlogo, "promotion" => $_POST['promotion'], "bonus" => $_POST['bonus'], "deposit" => $_POST['deposit'], "devices" => $checkBoxDevices, "title" => $_POST['title'], "url" => $_POST['url'], "description" => $desc, "country" => $_POST['country'], "popularity" => $_POST['popularity'], "commission" => $_POST['commission'], "master_link" => $_POST['master_link'], "pm_link" => $_POST['pm_link'], "wusa_link" => $_POST['wusa_link'], "sd_link" => $_POST['sd_link'], "affiliate_network" => $_POST['affiliate_network'], "slug" => $slug,"facebook" => $_POST['facebook'], "twitter" => $_POST['twitter'], "youtube"=>['youtube']));

        $id = DB::insertId();
        DB::insert('sites_category', array("site_id" => $id, "category_id" => $_POST['category'], "position" => $_POST['position']));          DB::insert('sites_tab', array("site_id" => $id, "tab_id" => $_POST['tab'], "position" => $_POST['position']));  
        /* $qry = "INSERT INTO sites (category_id,logo, promotion,bonus,deposit,devices,position,title,url,
          description,country,popularity,commission,master_link,pm_link,wusa_link,sd_link,affiliate_network,slug )
          VALUES ('$_POST[category]','$logo', '$_POST[promotion]','$_POST[bonus]' , '$_POST[deposit]', '" . $checkBoxDevices . "', '$_POST[position]', '$_POST[title]', '$_POST[url]','$desc','$_POST[country]','$_POST[popularity]','$_POST[commission]','$_POST[master_link]','$_POST[pm_link]','$_POST[wusa_link]','$_POST[sd_link]','$_POST[affiliate_network]','$slug')";
          $rs = db::queryRaw($qry); */
    }
}
?>  
<div class="jumbotron">
    <h3>Add Site Detail</h3>

    <br>
    <form name="eform" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
            <tr>
                <th colspan="3" align='left'>Site Information</th>
            </tr>
            <tr>
                <td valign="top" align='left'>Category</td>
                <td colspan="2">
                    <select name="category">
                        <option>Select Category </option>
                        <?php
                        $categories = DB::query("SELECT * FROM category");

                        foreach ($categories as $category) {

                            $category_id = $category['id'];
                            $category_title = $category['name'];
                            if (($_GET['category'] == $category_id) && !empty( $_GET['category']) ) {
                                echo "<option value='$category_id' selected>$category_title</option>";
                            } else {
                                echo "<option value='$category_id' >$category_title</option>";
                            }
                        }
                        ?>  
                    </select>
                </td>
            </tr>
            
            <tr>
                <td valign="top" align='left'>Tab</td>
                <td colspan="2">
                    <select name="tab">
                        <option>Select Tab </option>
                        <?php
                        $tabs = DB::query("SELECT * FROM tab");

                        foreach ($tabs as $tab) {

                            $tab_id = $tab['id'];
                            $tab_title = $tab['name'];
                            if (($_GET['tab'] == $tab_id) && !empty( $_GET['tab']) ) {
                                echo "<option value='$tab_id' selected>$tab_title</option>";
                            } else {
                                echo "<option value='$tab_id' >$tab_title</option>";
                            }
                        }
                        ?>  
                    </select>
                </td>
            </tr>
            <tr>
                <td width="20%" align="left">Site Name</td>
                <td width="75%" colspan="2"><input name="title" type="text" value="" class="l" ></td>
            </tr>
            <tr>
                <td align='left'>Position</td>
                <td colspan="2"><input name="position" value="" type="text" class="l" ></td>
            </tr>  


            <tr>
                <td valign="top" align='left'>Logo</td>
                <td colspan="2"><input type="file" name="logo" class="l"></td>
            </tr>
            <tr>
                <td valign="top" align='left'>Round Logo</td>
                <td colspan="2"><input type="file" name="roundlogo" class="l"></td>
            </tr>
            <tr>
                <td align='left'>Promotion</td>
                <td colspan="2"><input name="promotion" value="" type="text" class="l" ></td>
            </tr>            
            <tr>
                <td align='left'>Bonus</td>
                <td colspan="2"><input name="bonus" value="" type="text" class="l" ></td>
            </tr>
            <tr>
                <td align='left'>Deposit</td>
                <td colspan="2"><input name="deposit" value="" type="text" class="l" ></td>
            </tr>
            <tr>
                <td align='left'>Url</td>
                <td colspan="2"><input name="url" value="" type="text" class="l" ></td>
            </tr>

            <tr>
                <td valign="top" align='left'>Site Description</td>
                <td align='left'>
                    <textarea  placeholder="description" name="description" id="description" cols="70" rows="10"></textarea></td>
            </tr>
            <tr>
                <td align='left'>Apps</td>
                <td><input type="checkbox" name="apps[]" value="Windows"  class="l" >   Windows
                    <input type="checkbox" name="apps[]" value="iOS"  class="l" >   iOS   
                    </br><input type="checkbox" name="apps[]" value="Android"  class="l" >   Android
                    <input type="checkbox" name="apps[]" value="Blackberry"  class="l" >     Blackberry</td>

            </tr> 
            <tr>
                <td align='left'>Devices</td>
                <td><input type="checkbox" name="devices[]" value="PC"  class="l" >   PC
                    <input type="checkbox" name="devices[]" value="Tablet"  class="l" >   Tablet   
                    </br><input type="checkbox" name="devices[]" value="Smartphone"  class="l" >   Smartphone


            </tr> 
            <tr>
                <td align='left'>Country</td>
                <td colspan="2"><input name="country" value="" type="text" class="l" ></td>
            </tr>
            <tr>
                <td align='left'>Popularity</td>
                <td colspan="2"><input name="popularity" value="" type="text" class="l" ></td>
            </tr> 
            <tr>
                <td align='left'>Commission</td>
                <td colspan="2"><input name="commission" value="" type="text" class="l" ></td>
            </tr> 
            <tr>
                <td align='left'>Master Link</td>
                <td colspan="2"><input name="master_link" value="" type="text" class="l" ></td>
            </tr>
            <tr>
                <td align='left'>Performance Marketier link</td>
                <td colspan="2"><input name="pm_link" value="" type="text" class="l" ></td>
            </tr> 
            <tr>
                <td align='left'>Whoosh USA_link</td>
                <td colspan="2"><input name="wusa_link" value="" type="text" class="l" ></td>
            </tr> 
            <tr>
                <td align='left'>Search Digital link</td>
                <td colspan="2"><input name="sd_link" value="" type="text" class="l" ></td>
            </tr> 
            <tr>
                <td align='left'>Affiliate Network</td>
                <td colspan="2"><input name="affiliate_network" value="" type="text" class="l" ></td>
            </tr>
            <tr>
                <td align='left'>Facebook Link</td>
                <td colspan="2"><input name="facebook" value="" type="text" class="l" ></td>
            </tr> 
            <tr>
                <td align='left'>Twitter Link</td>
                <td colspan="2"><input name="twitter" value="" type="text" class="l" ></td>
            </tr> 
            <tr>
                <td align='left'>Youtube Link</td>
                <td colspan="2"><input name="youtube" value="" type="text" class="l" ></td>
            </tr> 
            <!--                                  
            <tr>
                <td valign="top" align='left'>Flash sale Large Image</td>
                <td colspan="2"><input type="file" name="flashsale_large_img" class="l">
            </tr>

                                  <tr>
                                    <td width="15%" align="left">Page Title</td>
                                    <td width="75%" colspan="2"><input name="page_title" type="text" maxlength="65" value="<?= $_POST['page_title'] ?>" class="l" ></td>
                                  </tr>
                                  <tr>
                                    <td width="15%" align="left">Meta Description</td>
                                        <td width="75%" colspan="2"><input name="meta_desc" maxlength="200" type="text" value="<?= $_POST['meta_desc'] ?>" class="l" ></td>
                                  </tr>
                                  <tr>
                                    <td width="15%" align="left">Meta Keywords</td>
                                        <td width="75%" colspan="2"><input name="meta_keywords" maxlength="200" type="text" value="<?= $_POST['meta_keywords'] ?>" class="l" ></td>
                                  </tr>                      
            -->            


            <tr>
                <td>&nbsp;</td>
                <td  colspan="2"> <input name="submit" type="submit" class="button" value="Save Site Details" /> <input name="cancel" type="Button" value="Cancel" onclick="history.go(-1)" /></td>
            </tr>
        </table>
    </form>
</div>
<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script>tinymce.init({selector: 'textarea'});</script>

<?php
include "footer.php";
?>
