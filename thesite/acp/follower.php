<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require "twitter/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;

require_once 'header.php';
include_once("../twitter/config.php");
?>
<div class="jumbotron">
    <h3>Followers</h3>
    <form method="post"  enctype="multipart/form-data">
        <h4>Twitter Profile Page Url's</h4>

        <textarea  name="followers" rows="6" cols="60">

        </textarea><br/><br/><br/><br/>
        <h4>Show Total Users :</h4>
        <?php
        $users = DB::queryFirstRow("select * from users");
        $usersCount = DB::count();
        echo '<h4>' . $usersCount . '</h4>';
        ?>

        <h4>Quantity</h4>
        <input type='text' name='quantitytext' placeholder="Enter User Quantity">
        <input  name="submit" type="submit" class="button" value="Submit" /> 
    </form>
    <?php
    $quanity = '';
    $count = 0;
    global $connection;

    if (isset($_POST['submit'])) {
        $quantity = $_POST['quantitytext'];
        $query = db::query("select * from users ");
        foreach ($query as $q) {
            $username = $q['username'];          
            $token = $q['oauth_token'];
            $tokenSecret = $q['oauth_secret'];
            $text = trim($_POST['followers']);
            $text = explode("\n", $text);
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $token, $tokenSecret);
            $fetch = db::queryFirstRow("select * from proxy ORDER BY RAND() LIMIT 0,1");
            $proxy = $fetch['proxy'];
            $port = $fetch['port'];
            $proxyusername = $fetch['username'];
            $password = $fetch['password'];
            $proxyCount = $fetch['count'];
            $connection->setProxy(array(
                'CURLOPT_PROXY' => $proxy,
                'CURLOPT_PROXYUSERPWD' => '',
                'CURLOPT_PROXYPORT' => $port,
            ));
            // proxy End
            foreach ($text as $line) {
                createFriends($line);
            }
            $count++;            
            if ($count == $quantity) {
                
                break;
            }
        }
    }
    function createFriends($line) {
        try {
            global $connection;
            global $proxyCount;
            global $proxy;
            //For Proxy
            $connection->post('friendships/create', array('screen_name' => $line));
        } catch (Exception $e) {
             
           
            $proxyCount ++;
            DB::update('proxy', array(
                'count' => $proxyCount
                    ), "proxy=%s", $proxy);
            
            if ($proxyCount == 5) {
                DB::delete('proxy', "proxy=%s", $proxy);
            }
            echo $e->getMessage()."      ";
        }
    }
    ?>


</div>
