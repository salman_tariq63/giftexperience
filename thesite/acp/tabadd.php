<html><head>
    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script>
        tinymce.init({selector:'textarea'});
</script>
        
    </head></html>
<?php
require_once 'header.php';
//$agents = DB::query("select id, name from agents");

// Edit
if (!empty($_GET['id'])) {
    $cloneData = DB::queryFirstRow("select * from tab where id=%d", $_GET['id']);
//    $cloneData['phonenumbers'] = unserialize ($cloneData['phonenumbers']);
    $formAction = "Modify";
} else {
    $formAction = "Save";
}

if (!empty($_POST['Save'])) {

//    if (!empty($_POST['activeAgents'])) {
//        $activeAgents = implode(",", $_POST['activeAgents']);
//    }
   $slug = formatstr($_POST['name']);

    DB::insert('tab', array(
        "name" => $_POST['name'],
        "slug" => $slug,
         "description" =>$_POST['description'],
//        "active" => $_POST['active'],
//        "activeAgents" => $activeAgents
    ));

    header("location:tab.php");
}

if (!empty($_POST['Modify'])) {

//    if (!empty($_POST['activeAgents'])) {
//        $activeAgents = implode(",", $_POST['activeAgents']);
//    }
        $slug_edit = formatstr($_POST['name']);

    DB::update('tab', array(
        "name" => $_POST['name'],
        "slug" => $slug_edit ,
        "description" =>$_POST['description']
//        "active" => $_POST['active'],
//        "activeAgents" => $activeAgents
            ), "id=%d", $_POST['id']);

    header("location: index.php");
}
?>

<div class="jumbotron">
    <?php 
    if($formAction == 'Save'){ ?>
    <h3>Add Category</h3>
    <?php } else { ?>
    <h3>Edit Category</h3>
    <?php } ?>
    <div>
        <?php
//        displayErrors($errors);
//        displayMessages($messages);
        ?>
    </div>

    <form method="post" class="form-horizontal">

        <div class="form-group">
          <label>Name</label><input type="text" name="name" class="form-control" placeholder="Enter Category Name" required="" value="<?=$cloneData['name']?>">
        </div>
        <div class="form-group">
               <?php if($formAction == 'Save'){ ?>
               <textarea  placeholder="description" name="description" id="description"  cols="50" rows="10" value="<?=$cloneData['description']?>"></textarea> <?php } else {  ?>
               <textarea  placeholder="description" name="description" id="description"  cols="50" rows="10" value=""><?=$cloneData['description']?></textarea> <?php  }?>
        </div>


        <div class="form-group">
            <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
            <button type="submit" name="<?= $formAction ?>" value="<?= $formAction ?>" class="btn btn-default"><?= $formAction ?></button>
        </div>
    </form>

</div>

<?php
require 'footer.php';
?>

