<?php require_once 'header.php'; 
?>
<?php
if (!empty($_GET['id'])) {
    $proxyData = DB::queryFirstRow("select * from proxy where id=%d", $_GET['id']);
//    $cloneData['phonenumbers'] = unserialize ($cloneData['phonenumbers']);
    $formAction = "Modify";
} else {
    $formAction = "Save";
}

if (!empty($_POST['Save'])) {

    DB::insert('proxy', array(
        "proxy" => $_POST['proxy'],
        "port" => $_POST['port'],
        "username" => $_POST['username'],
        "password" => $_POST['password']
    ));

    echo "<script>window.open('proxy.php','_self') </script>";
}

if (!empty($_POST['Modify'])) {


    DB::update('proxy', array(
        "proxy" => $_POST['proxy'],
        "port" => $_POST['port'],
        "username" => $_POST['username'],
        "password" => $_POST['password']
            ), "id=%d", $_POST['id']);

     echo "<script>window.open('proxy.php','_self') </script>";
}
?>
<html><head>
        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        <script>
            tinymce.init({selector: 'textarea'});
        </script>

    </head></html>

<div class="jumbotron">
    <?php if ($formAction == 'Save') { ?>
        <h3>Add Proxy</h3>
    <?php } else { ?>
        <h3>Edit Proxy</h3>
    <?php } ?>
    <div>
        <?php
//        displayErrors($errors);
//        displayMessages($messages);
        ?>
    </div>

    <form method="post" class="form-horizontal">



        <?php if ($formAction == 'Save') { ?>
            <div class="form-group">
                <label>Proxy</label><input type="text" name="proxy" class="form-control" placeholder="Enter Proxy" required="" value="">
            </div>
            <div class="form-group">
                <label>Port</label><input type="text" name="port" class="form-control" placeholder="Enter Port" required="" value="">
            </div><div class="form-group">
                <label>Username</label><input type="text" name="username" class="form-control" placeholder="Enter User name" required="" value="">
            </div><div class="form-group">
                <label>Password</label><input type="password" name="password" class="form-control" placeholder="Enter Password" required="" value="">
            </div>
        <?php } else { ?>
            <div class="form-group">
                <label>Proxy</label><input type="text" name="proxy" class="form-control" placeholder="Enter Proxy" required="" value="<?= $proxyData['proxy'] ?>">
            </div>
            <div class="form-group">
                <label>Port</label><input type="text" name="port" class="form-control" placeholder="Enter Port" required="" value="<?= $proxyData['port'] ?>">
            </div><div class="form-group">
                <label>Username</label><input type="text" name="username" class="form-control" placeholder="Enter User name" required="" value="<?= $proxyData['username'] ?>">
            </div><div class="form-group">
                <label>Password</label><input type="password" name="password" class="form-control" placeholder="Enter Password" required="" value="<?= $proxyData['password'] ?>">
            </div>
           
        <?php  } ?>
         <div class="form-group">
                <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
                <button type="submit" name="<?= $formAction ?>" value="<?= $formAction ?>" class="btn btn-default"><?= $formAction ?></button>
            </div>
        </form>

    </div>

    <?php
    require 'footer.php';
    ?>

