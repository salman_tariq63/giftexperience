<?php
require_once 'header.php';
if (!empty($_POST['go'])) {
    if (!empty($_POST['id'])) {
        
            foreach ($_POST['id'] as $proxyId) {
                DB::delete('proxy', "id=%d", $proxyId);
                $messages[] = "Proxy Deleted!";
            }
        
    }
}
$proxy = DB::query("select * from proxy");
?>

<div class="jumbotron">
    <h3>Proxies</h3>
     <a href="bulk.php">Add Bulk Proxies</a>
     
    <a href="proxyadd.php">Add new</a>

    <div>
        <?php
        //displayErrors($errors);
        //displayMessages($messages);
        ?>
    </div>

    <form method="post" class="form-horizontal">

        <table class="table table-striped" width="100%">
            <thead>
                <tr align="right">
                    <td colspan="10">
                        
                        <button type="submit" name="go" value="Go" class="btn btn-default btn-xs">Delete</button>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Proxy</th>
                    <th>Port</th>
                    <th>Username</th>
                    <th>Password</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($proxy as $proxies) {
                    ?>
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?= $proxies['id'] ?>"></td>
                        <td><?= $proxies['id'] ?></td>
                        <td><?= $proxies['proxy'] ?></td>
                        <td><?= $proxies['port'] ?></td>
                        <td><?= $proxies['username'] ?></td>
                        <td><?= $proxies['password'] ?></td>
                       
                        <td>
                            <a href="proxyadd.php?id=<?= $proxies['id'] ?>">
                                Edit</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>        
            </tbody>
        </table>


    </form>
</div>
</div>

<?php
require 'footer.php';
?>

