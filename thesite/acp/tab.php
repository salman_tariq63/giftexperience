<?php
require_once 'header.php';
if (!empty($_POST['go'])) {
    if (!empty($_POST['id'])) {
        
            foreach ($_POST['id'] as $tabId) {
                DB::delete('tab', "id=%d", $tabId);
                $messages[] = "Tab Deleted!";
            }
        
    }
}
$tab = DB::query("select * from tab");
?>

<div class="jumbotron">
    <h3>Tabs</h3>
    <a href="tabadd.php">Add new</a>

    <div>
        <?php
        //displayErrors($errors);
        //displayMessages($messages);
        ?>
    </div>

    <form method="post" class="form-horizontal">

        <table class="table table-striped" width="100%">
            <thead>
                <tr align="right">
                    <td colspan="10">
                        
                        <button type="submit" name="go" value="Go" class="btn btn-default btn-xs">Delete</button>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <th>Id</th>
                    <th>Name</th>

                    <th>Edit</th>
                    <th></th>
                    <th></th>

                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($tab as $clone) {
                    ?>
                    <tr>
                        <td><input type="checkbox" name="id[]" value="<?= $clone['id'] ?>"></td>
                        <td><?= $clone['id'] ?></td>
                        <td><a href='tabadd.php?id=<?= $clone['id'] ?>'><?= $clone['name'] ?></a></td>
                        <td>
                            <a href="tabadd.php?id=<?= $clone['id'] ?>">
                                Edit</a></td>
                        <td>
                            <a href="sites_add.php?tab=<?= $clone['id'] ?>">Add New Site To tab</a>

                        </td>
                        <td>
                           <a href="exsistingSiteToTab.php?tab=<?= $clone['id']?>">Add Existing Site To tab</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>        
            </tbody>
        </table>


    </form>
</div>
</div>

<?php
require 'footer.php';
?>

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

