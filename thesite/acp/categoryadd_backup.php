<html><head>
        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        <script>
            tinymce.init({selector: 'textarea'});
        </script>

    </head></html>
<?php
require_once 'header.php';

// Edit
if (!empty($_GET['id'])) {
    $catData = DB::queryFirstRow("select * from category where id=%d", $_GET['id']);
    $formAction = "Modify";
} else {
    $formAction = "Save";
}

if (!empty($_POST['Save'])) {

    $slug = formatstr($_POST['name']);

    DB::insert('category', array(
        "name" => $_POST['name'],
        "slug" => $slug,
        "description" => $_POST['description'],
    ));

    header("location:category.php");
}

if (!empty($_POST['Modify'])) {

    $slug_edit = formatstr($_POST['name']);

    DB::update('category', array(
        "name" => $_POST['name'],
        "slug" => $slug_edit,
        "description" => $_POST['description']
            ), "id=%d", $_POST['id']);

    header("location:category.php");
}
?>

<div class="jumbotron">
    <?php if ($formAction == 'Save') { ?>
        <h3>Add Category</h3>
    <?php } else { ?>
        <h3>Edit Category</h3>
    <?php } ?>
    <div>
        <?php
//        displayErrors($errors);
//        displayMessages($messages);
        ?>
    </div>

    <form method="post" class="form-horizontal">

        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control" placeholder="Enter Category Name" required="" value="<?=$catData['name']?>">
        </div>
        <div class="form-group">
            <textarea  placeholder="Please enter details/content about this site" name="description" id="description" cols="50" rows="10">
                <?=$catData['description']?>
            </textarea></td>
        </div>


        <div class="form-group">
            <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
            <button type="submit" name="<?= $formAction ?>" value="<?= $formAction ?>" class="btn btn-default"><?= $formAction ?></button>
        </div>
    </form>

</div>

<?php
require 'footer.php';
?>

