<?php
include "header.php";
//echo "<pre>";
//print_r($_GET);


$qry = "select id,title, roundlogo,roundlogoComments from sites where roundlogoApproval = 2";
$rs = db::query($qry);

$counter = db::count();
?>
<div class="jumbotron">
    
    <?php if($counter == 0){ ?>
          <h3>No Logos</h3>
<?php
    }else { ?>
    <h3>Pending Logos</h3>
    <p>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-striped">
        <thead>
            <tr> 
                <th>Site</th>
                <th>Description</th>
                <th>Approve</th>
                <th>Comments</th>
            </tr>
        </thead>
        <form>
        <?php
        foreach ($rs as $row){
            ?>      
            <tr class="row<?= @$i++ % 2 ?>">
                <td>
                    <?php echo $row['title']; ?>
                </td>
                <td>
                    <img src="/images/roundlogo/<?php echo $row['roundlogo']; ?>">
                </td>
                <td>
                    Dissapproved
                </td>
                <td>
                    <?= $row['roundlogoComments'] ?>
                </td>
                <td>
                    <a href="sites_edit.php?id=<?=$row['id'] ?>&task=roundlogo" > Fix it </a>
                </td>
            </tr>
            <?php
        }
        ?>            
    </table>
    </form>
    <p></p>
    <?php } ?>
</div>
<?php
include "footer.php";
?>
