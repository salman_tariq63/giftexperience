<?php
require_once 'header.php';

if(!empty($_POST['save'])){
    if(!empty($_POST['newPassword'])){
        if($_POST['newPassword']<>$_POST['retypeNewPassword']){
            $errors[] = "Password and retype password fields did not match";
        }
        else{
        $password = $_POST['newPassword'];
        $email = $_POST['editorEmail'];
        $role = 'editor';
        setSettings($password,$email,$role);
       
        }
        
      
    }
}

$editorEmail = getSettings("editorEmail");
?>

      <div class="jumbotron">
        <h3>Editor Settings</h3>
        <div>
       
        </div>
        
        <form method="post" class="form-horizontal">
            <div class="form-group">
                <h4>Change Editor info</h4>
            </div>
            
            <div class="form-group">
                <label>Editor email address</label><input type="email" name="editorEmail" required="" class="form-control" placeholder="Editor Email Address" value="<?=$editorEmail?>">
            </div>
            
            <div class="form-group">
                <label>New password</label><input type="password" name="newPassword" class="form-control" placeholder="New Password">
            </div>
            
            <div class="form-group">
                <label>Retype new password</label><input type="password" name="retypeNewPassword" class="form-control" placeholder="Rettype New Password">
            </div>
                        
            <div class="form-group">
                <button type="submit" name="save" value="save" class="btn btn-default">Save</button>
            </div>
        </form>
        </div>
      </div>

<?php
require 'footer.php';
?>
