<?php
    require_once '../inc/initDb.php';
    require_once 'functions.php';
    
    if(!empty($_POST['signin'])){
        
        if($_POST['adminUser'] == getSettings('adminUser') and $_POST['adminPassword'] == getSettings('adminPassword')){
           startAdminSession('admin');
            header("location:index.php");
        }
        else if($_POST['adminUser'] == getSettings('editorUser') and $_POST['adminPassword'] == getSettings('editorPassword')){
            startAdminSession('editor');
            header("location:index.php");
        }
        else {
            $errors[] = "Login password mismatch!";
        }
    }
?>
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Signin Bingo 88</title>
    
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
      <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>       
        <label for="inputUser" class="sr-only">User Name</label>
            <input type="text" id="adminUser" name="adminUser" class="form-control" placeholder="User" required="" autofocus="">
        <label for="adminPassword" class="sr-only">Password</label>
            <input type="password" id="adminPassword" name="adminPassword" class="form-control" placeholder="Password" required="">
        <input type="submit" name="signin" value="Sign in" class="btn btn-lg btn-primary btn-block">
        <p>
            <a href="passwordreminder.php">Password Reminder</a>
        </p>

      </form>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  
</body></html>