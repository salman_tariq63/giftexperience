<?php
include "header.php";
$error = "";
$c = 0;
if (isset($_GET['task'])) {
    if ($_GET['task'] == 'logo') {
        $id = $_GET['id'];
        $updateQuery = "update sites set
             logoApproval = 0 where id='$id'";

        db::queryRaw($updateQuery);
    }
    if ($_GET['task'] == 'description') {
        $id = $_GET['id'];
        $updateQuery = "update sites set
             descApproval = 0 where id='$id'";

        db::queryRaw($updateQuery);
    }
    if ($_GET['task'] == 'roundlogo') {
        $id = $_GET['id'];
        $updateQuery = "update sites set
             roundlogoApproval = 0 where id='$id'";

        db::queryRaw($updateQuery);
    }
}
if (!empty($_POST['submit'])) {
    if (empty($_POST['title']) and empty($_POST['promotion'])) {
        $error = "Please enter Site Title and promotion";
    } else {
        $title = htmlentities($_POST['title'], ENT_QUOTES, "UTF-8");

        $id = $_POST['id'];

        // for logo
        $logo = $_FILES['logo']['name'];
        $temp_name1 = $_FILES['logo']['tmp_name'];
        $isUploaded = move_uploaded_file($temp_name1, "../images/logo/$logo");
        if ($isUploaded) {
            DB::update('sites', array("logo" => $logo), "id=%i", "$id");
        }


        // For Round Logo
        $roundlogo = $_FILES['roundlogo']['name'];
        $temp_name2 = $_FILES['roundlogo']['tmp_name'];
        $isUploaded = move_uploaded_file($temp_name2, "../images/roundlogo/$roundlogo");
        if ($isUploaded) {
            DB::update('sites', array("roundlogo" => $roundlogo), "id=%i", "$id");
        }

        $temp = $_POST['oldDescription'];
        if ($temp == $_POST['description']) {
            
        } else {
            DB::update('sites', array("descApproval" => 0), "id=%i", "$id");
        }
        $slug = formatstr($_POST['title']);
        //$checkBox = @implode(',', $_POST['apps']);
        $checkBoxDevice = @implode(',', $_POST['devices']);
        DB::update('sites', array("promotion" => $_POST['promotion'], "bonus" => $_POST['bonus'], "deposit" => $_POST['deposit'], "devices" => $checkBoxDevice, "title" => $_POST['title'], "url" => $_POST['url'], "description" => $_POST['description'], "country" => $_POST['country'], "popularity" => $_POST['popularity'], "commission" => $_POST['commission'], "master_link" => $_POST['master_link'], "pm_link" => $_POST['pm_link'], "wusa_link" => $_POST['wusa_link'], "sd_link" => $_POST['sd_link'], "affiliate_network" => $_POST['affiliate_network'], "slug" => $slug, "facebook" => $_POST['facebook'], "twitter" => $_POST['twitter'], "youtube" => $_POST['youtube']), "id=%i", "$id");
        
        if ($error == "") {
            header("location:sites_edit.php?id=$id");
        }
    }
} else {
    //$qry = "select sites.*, category.id as cid, category.name as cn from sites left join category on sites.category_id = category.id where sites.id={$_GET['id']}";
    $qry = "select * from sites where id={$_GET['id']}";
    $_POST = db::queryFirstRow($qry);
    //print_r($_POST);
//    if (!empty($_POST['apps'])){
//        $os = explode(",", $_POST['apps']);
//    }
//    else{
//        $os = array();
//    }
    if (!empty($_POST['devices'])) {
        $d = explode(",", $_POST['devices']);
    } else {
        $d = array();
    }
}
//print_r($_POST);
?>
<div class="jumbotron">
    <h3>Edit Site Information</h3>
    <form name="eform" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $_POST['id'] ?>">
        <table width="70%" height="100%" border="0" cellpadding="0" cellspacing="0" class="table">
            <tr> <?php if ($c == 1) { ?>
                    <th colspan="3" align='left'> 
                        Your information have been saved Successfully
                    </th>
                    <?php
                }
                $c = 0;
                ?>
                <th colspan="3" align='left'>Site Information</th>
            </tr>
            <tr>
                <td width="20%" align="left">Site Name</td>
                <td width="75%" colspan="2"><input name="title" type="text" value="<?= $_POST['title'] ?>" class="l"></td>
            </tr>
            
            <tr>
                <td valign="top" align='left'>Category</td>
                <td colspan="2">
                    <select name="category">
                        <?php
                        $categories = DB::query("SELECT * FROM category");
                        foreach ($categories as $category) {
                            $category_id = $category['id'];
                            $category_title = $category['name'];

                            if ($_POST['cid'] == $category_id)
                                echo "<option value='$category_id' selected>$category_title</option>";
                            else
                                echo "<option value='$category_id'>$category_title</option>";
                        }
                        ?>  
                    </select>
                </td>
            </tr>
            <tr>
                <td valign="top" align='left'>Tab</td>
                <td colspan="2">
                    <select name="tab">
                        <?php
                        $tabs = DB::query("SELECT * FROM tab");

                        foreach ($tabs as $tab) {

                            $tab_id = $tab['id'];
                            $tab_title = $tab['name'];

                            if ($_POST['cid'] == $tab_id)
                                echo "<option value='$tab_id' selected>$tab_title</option>";
                            else
                                echo "<option value='$tab_id'>$tab_title</option>";
                        }
                        ?>  
                    </select>
                </td>
            </tr>
            <tr>
                <td valign="top" align='left'>Logo</td>
                <td colspan="2">
                    <input type="file" name="logo" class="l"><br>
                    <img src="../images/logo/<?php echo $_POST['logo']; ?>" width="60" height="60"></td>
            </tr>
            <tr>
                <td valign="top" align='left'>Round Logo</td>

                <td colspan="2">
                    <input type="file" name="roundlogo" class="l"><br>
                    <img src="../images/roundlogo/<?php echo $_POST['roundlogo']; ?>" width="32" height="32"></td>
            </tr>
            <tr>
                <td align='left'>Promotion</td>
                <td colspan="2"><input name="promotion" value="<?= $_POST['promotion'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>Bonus</td>
                <td colspan="2"><input name="bonus" value="<?= $_POST['bonus'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>Deposit</td>
                <td colspan="2"><input name="deposit" value="<?= $_POST['deposit'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>Url</td>
                <td colspan="2"><input name="url" value="<?= $_POST['url'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td valign="top" align='left'>Site Description</td>
                <td align='left'>
                    <?php ?>
                    <input type="hidden" name="oldDescription" value="<?= htmlentities($_POST['description']) ?>"></input>  

                    <textarea  placeholder="description" name="description" id="description"><?= htmlentities($_POST['description']) ?></textarea></td>
            </tr>
<!--            <tr>
                <td align='left'>Apps</td>
                <td><input type="checkbox" name="apps[]"  //in_array('Windows', $os) === false ? '' : 'checked' ?> value="Windows"  class="l">   Windows
                    <input type="checkbox" name="apps[]"  //in_array('iOS', $os) === false ? '' : 'checked' ?> value="iOS"  class="l">   iOS   
                    </br><input type="checkbox" name="apps[]"  //in_array('Android', $os) === false ? '' : 'checked' ?> value="Android"  class="l">   Android
                    <input type="checkbox" name="apps[]"  //in_array('Blackberry', $os) === false ? '' : 'checked' ?> value="Blackberry"  class="l">     Blackberry</td>

            </tr> -->
            <tr>
                <td align='left'>Devices</td>
                <td><input type="checkbox" name="devices[]" <?= in_array('PC', $d) === false ? '' : 'checked' ?> value="PC"  class="l">   PC
                    <input type="checkbox" name="devices[]" <?= in_array('Tablet', $d) === false ? '' : 'checked' ?> value="Tablet"  class="l">  Tablet   
                    </br><input type="checkbox" name="devices[]" <?= in_array('Smartphone', $d) === false ? '' : 'checked' ?> value="Smartphone"  class="l">   Smartphone
                </td>

            </tr> 
            <tr>
                <td align='left'>Country</td>
                <td colspan="2"><input name="country" value="<?= $_POST['country'] ?>" type="text" class="l"></td>
            </tr> 

            <tr>
                <td align='left'>Popularity</td>
                <td colspan="2"><input name="popularity" value="<?= $_POST['popularity'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>Commission</td>
                <td colspan="2"><input name="commission" value="<?= $_POST['commission'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>Master Link</td>
                <td colspan="2"><input name="master_link" value="<?= $_POST['master_link'] ?>" type="text" class="l"></td>
            </tr>
            <tr>
                <td align='left'>PM_link</td>
                <td colspan="2"><input name="pm_link" value="<?= $_POST['pm_link'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>WUSA_link</td>
                <td colspan="2"><input name="wusa_link" value="<?= $_POST['wusa_link'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>SD_link</td>
                <td colspan="2"><input name="sd_link" value="<?= $_POST['sd_link'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>Affiliate Network</td>
                <td colspan="2"><input name="affiliate_network" value="<?= $_POST['affiliate_network'] ?>" type="text" class="l"></td>
            </tr>
            <tr>
                <td align='left'>Facebook Link</td>
                <td colspan="2"><input name="facebook" value="<?= $_POST['facebook'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>Twitter Link</td>
                <td colspan="2"><input name="twitter" value="<?= $_POST['twitter'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td align='left'>Youtube Link</td>
                <td colspan="2"><input name="youtube" value="<?= $_POST['youtube'] ?>" type="text" class="l"></td>
            </tr> 
            <tr>
                <td>&nbsp;</td>
                <td  colspan="2"> <input name="submit" type="submit" class="button" value="Save Site Info" /> <input name="cancel" type="Button" value="Cancel" onclick="history.go(-1)" /></td>
            </tr>
        </table>

    </form>
</div>
<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script>tinymce.init({selector: 'textarea'});</script>

<?php
include "footer.php";
?>
