<?php
require_once 'header.php';
//$agents = DB::query("select id, name from agents");
// Edit
if (!empty($_GET['id'])) {
    $cloneData = DB::queryFirstRow("select * from clones where id=%d", $_GET['id']);
//    $cloneData['phonenumbers'] = unserialize ($cloneData['phonenumbers']);
    $formAction = "Modify";
} else {
    $formAction = "Save";
}

if (!empty($_POST['Save'])) {
    DB::insert('clones', array(
        "name" => $_POST['name'],
        "category_id" => $_POST['category'],
        "active" => $_POST['active'],
        "overlay" => $_POST['overlay'],
        "affiliate" => $_POST['affiliate'],
        "cling_url" => $_POST['cling_url']
            // ,"activeAgents" => $activeAgents
    ));

    header("location:clones.php");
}

if (!empty($_POST['Modify'])) {
    DB::update('clones', array(
        "name" => $_POST['name'],
        "active" => $_POST['active'],
        "overlay" => $_POST['overlay'],
        "affiliate" => $_POST['affiliate'],
        "cling_url" => $_POST['cling_url']
            //,"activeAgents" => $activeAgents
            ), "id=%d", $_POST['id']);

    header("location:clones.php");
}
?>

<div class="jumbotron">
    
    <?php if ($formAction == 'Save') { ?>
        <h3>Add Clone</h3>
    <?php } else { ?>
        <h3>Edit Clone</h3>
    <?php } ?>
        
    <form method="post" class="form-horizontal">

        <div class="form-group">
            <label>Clone Name</label><input type="text" name="name" class="form-control" placeholder="Name" required="" value="<?= $cloneData['name'] ?>">
        </div>


        <div class="form-group">
            <label>Category (This will be main category of this clone displayed on homepage)</label>
            <select name="category" class="form-control">
                <option>Select Category </option>
                <?php
                $categories = DB::query("SELECT * FROM category");

                foreach ($categories as $category) {

                    $category_id = $category['id'];
                    $category_title = $category['name'];
                    echo "<option value='$category_id'>$category_title</option>";
                }
                ?>  
            </select>
        </div>
        
        <div class="form-group">
            <label>Affiliates (Select which outgoing affiliate link is applied to sites for this clone)</label>
            <select name="affiliate" class="form-control">
                <option value="<?= $cloneData['affiliate']; ?>"> </option>
                
                <option value="master_link" <?=$cloneData['affiliate']=='master_link' ? 'selected' : '' ?>>Master Link</option>
                <option value="pm_link" <?=$cloneData['affiliate']=='pm_link' ? 'selected' : '' ?>>Performance Marketeers Link</option>
                <option value="wusa_link" <?=$cloneData['affiliate']=='wusa_link' ? 'selected' : '' ?>>Whoosh USA Link</option>
                <option value="sd_link" <?=$cloneData['affiliate']=='sd_link' ? 'selected' : '' ?>>Search Digital Link</option>
            </select>
        </div>
        <div class="form-group">
            <input type="checkbox" id="active" class="checkbox-inline" name="active" class="checkbox" value="1" <?= $cloneData['active'] == 1 ? "checked" : "" ?>>
            <label for="active">Site Active (Enable/Disable site)</label>            
        </div>
        <div class="form-group">
            <label for="overlay">Overlay (Display Cling Overlay)</label>
            <input type="checkbox" id="overlay" name="overlay" class="checkbox" value="1" <?= $cloneData['overlay'] == 1 ? "checked" : "" ?>>
        </div>
        <div class="form-group">
            <label>Cling Url</label>
            <textarea  placeholder="Enter Cling Url" name="cling_url" class="form-control" id="description" cols="50" rows="10"><?=$cloneData['cling_url']?></textarea></td>
        </div>
        <div class="form-group">
            <input type="hidden" name="id" value="<?= @$_GET['id'] ?>">
            <button type="submit" name="<?= $formAction ?>" value="<?= $formAction ?>" class="btn btn-default"><?= $formAction ?></button>
        </div>
    </form>
</div>
</div>
<?php
require 'footer.php';
?>
