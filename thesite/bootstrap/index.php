<?php include '../db.php';
$rpp = 24; // results per page
$adjacents = 4;
$page = intval($_GET["page"]);
if ($page <= 0) {
    $page = 1;
}
$url = $_SERVER['REQUEST_URI'];
$reload = "bootstrap/index.php?tag=" . $_GET['tag'];
print_r($_GET);
exit;
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../bootstrap/css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
         .dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 10px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}
            .dropdown {
    position: relative;
    
    padding-left: 0;
    margin-bottom: 20px;
}
            .dropdown-menu {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
}
.dropdown:hover .list-group-item {
    background-color: #3e8e41;
}
.dropdown:hover .dropdown-menu {
    display: block;
}
.dropdown-menu a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

    ul.setPaginate li.setPage {
        padding: 15px 10px;
        font-size: 14px;
    }

    ul.setPaginate {
        margin: 0px;
        padding: 0px;
        height: 100%;
        overflow: hidden;
        font: 12px 'Tahoma';
        list-style-type: none;
    }

    ul.setPaginate li.dot {
        padding: 3px 0;
    }

    ul.setPaginate li {
        float: left;
        margin: 0px;
        padding: 0px;
        margin-left: 5px;
    }

    ul.setPaginate li,
    ul.setPaginate li a {
        border: 1px solid #cccccc;
        color: #999999;
        display: inline-block;
        font: 15px/25px Arial, Helvetica, sans-serif;
        margin: 5px 3px 0 0;
        padding: 0 5px;
        text-align: center;
        text-decoration: none;
    }

    ul.setPaginate li:hover,
    ul.setPaginate li.current {
        background: none repeat scroll 0 0 #0d92e1;
        color: #ffffff;
        text-decoration: none;
    }

    ul.setPaginate li a,
    ul.setPaginate li {
        color: black;
        display: block;
        text-decoration: none;
        padding: 5px 8px;
        text-decoration: none;
    }

    ul.setPaginate li a {
        padding: 0;
        border: none;
        margin: 0;
    }

    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="../js/jquery.bxSlider.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
 $('#slider-2').bxSlider({
        pager: true,
        pagerType:'short',
        controls: true,
        moveSlideQty: 3,
        displaySlideQty: 3
    });
    });
</script>
   
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Start Bootstrap</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Shop Name</p>
                <div class="list-group">
                        <?php
    $menu="select * from parent_cats where parent_id=0";
    $rsmen=  mysqli_query($conn, $menu);
    while($rwmen=  mysqli_fetch_assoc($rsmen)){
        
    ?>
                    <div class="dropdown">
                    <a href="/<?php echo $rwmen['slug']; ?>/page=0" class="list-group-item"><?php echo $rwmen['name'];?>
    </a>
                    <?php
    $drp="select * from parent_cats where parent_id=".$rwmen['id'];
    $rsdrp=  mysqli_query($conn, $drp);
    while($rwdrp=  mysqli_fetch_assoc($rsdrp)){
    ?>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="margin-top:0px !important" >
        
         
        
       
        <li role="presentation"><a role="menuitem" tabindex="-1" href="/<?php echo $rwdrp['slug']; ?>/page=0"  ><?php echo $rwdrp['name']; ?></a></li>
      

    </ul>
                                        <?php }?>
                    </div>
    <?php }?>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">
                    
                   <div  >
<?php 
$qry="select * from products where 1=1";
if(!empty($_GET['tag'])){
   $tags=$_GET['tag'];
   $slug="select * from parent_cats where slug='$tags'";
   $rsslg=mysqli_query($conn, $slug);
   //echo
    while($rowslg=  mysqli_fetch_assoc($rsslg)){
       $tagref=$rowslg['tags'];
       $qry.=" and match(product_name) against('$tagref') order by match(product_name) against('$tagref')";
   }
//echo $qry;

   $rssr=mysqli_query($conn, $qry);
   $tcount = mysqli_num_rows($rssr);
    $tpages = ($tcount) ? ceil($tcount / $rpp) : 1; // total pages, last page number
                                $count = 0;
                                $i = ($page - 1) * $rpp;
                                echo $i;
                                echo $tcount;
                                while (($count < $rpp) && ($i < $tcount)) {
                                    
                                    mysqli_data_seek($rssr, $i);
   while($rowrs=  mysqli_fetch_assoc($rssr)){
       
       $dataproduct=$rowrs;
   }
   print_r($dataproduct);
   ?>
                    <div  class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="<?php echo $rowrs["aw_image_url"]; ?>" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$<?php echo $rowrs['search_price'];?></h4>
                                <div style="width:50px">
                                <h4><a href="#"><?php echo $rowrs['product_name'];?></a>
                                </div>
                                </h4>
                                <p><?php echo substr($rowrs["description"], 0, 50) . "....." ?>  <a target="_blank" href="http://www.bootsnipp.com">Bootsnipp - http://bootsnipp.com</a>.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">15 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </p>
                            </div>
                        </div>
                    </div>

   <?php 
   $i++;
                                    $count++;
                                
                                
}}?>
                    </div>
                    <div class="yuiuy">
                            <?php
                            if ($tcount > 0) {
                                include("/pagination.php");
                                echo paginate_three(
                                        $reload, $page, $tpages, $adjacents
                                );
                            }
                            ?>
                        </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <h4><a href="#">Like this template?</a>
                        </h4>
                        <p>If you like this template, then check out <a target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">this tutorial</a> on how to build a working review system for your online store!</p>
                        <a class="btn btn-primary" target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">View Tutorial</a>
                    </div>
                 
                     </div>
              

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
