<?php
  function paginate_three($reload, $page, $tpages, $adjacents) {
	
	$prevlabel = "<";
	$nextlabel = ">";
	
	$out = "<ul class=\"setPaginate\">";
	
	// previous
	if($page==1) {
		$out.= "<li><<</li>";
		$out.= "<li>" . $prevlabel . "</li>";
	}
	elseif($page==2) {
		if (isset($_GET['s'])) {
			$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."&page=1\"><<</a></li>";
			$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."\">" . $prevlabel . "</a></li>";
		} else {
			$out.= "<li><a href=\"" .$reload . "&page=1\"><<</a></li>";
			$out.= "<li><a href=\"" .$reload . "\">" . $prevlabel . "</a></li>";		
		}
	}
	else {
		if (isset($_GET['s'])) {
			$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."&page=1\"><<</a></li>";
			$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."&page=" . ($page-1) . "\">" . $prevlabel . "</a></li>";
		} else {
			$out.= "<li><a href=\"" .$reload . "&page=1\"><<</a>\n";
			$out.= "<li><a href=\"" .$reload . "&page=" . ($page-1) . "\">" . $prevlabel . "</a></li>";		
		}
	}
	
	// first
	if($page>($adjacents+1)) {
		if (isset($_GET['s'])) {
			$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."\">1</a></li>";
		} else {
			$out.= "<li><a href=\"" .$reload . "/".$_GET['s']."\">1</a></li>n";
		}
	}
	
	// interval
	if($page>($adjacents+2)) {
		$out.= "...\n";
	}
	
	// pages
	$pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
	$pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
	for($i=$pmin; $i<=$pmax; $i++) {
		if($i==$page) {
			$out.= "<li class=\"current\">" . $i . "</li>";
		}
		elseif($i==1) {
			if (isset($_GET['s'])) {
				$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."\">" . $i . "</a></li>";
			} else {
				$out.= "<li><a href=\"" .$reload . "\">" . $i . "</a></li>";
			}
		}
		else {
			if (isset($_GET['s'])) {
				$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."&page=" . $i . "\">" . $i . "</a></li>";
			} else {
				$out.= "<li><a href=\"" .$reload . "&page=" . $i . "\">" . $i . "</a></li>";
			}
		}
	}
	
	// interval
	if($page<($tpages-$adjacents-1)) {
		$out.= "<li>...</li>";
	}
	
	// last
	if($page<($tpages-$adjacents)) {
		if (isset($_GET['s'])) {
			$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."&page=" . $tpages . "\">" . $tpages . "</a></li>";
		} else {
			$out.= "<li><a href=\"" .$reload . "&page=" . $tpages . "\">" . $tpages . "</a></li>";
		}
	}
	
	// next
	if($page<$tpages) {
		if (isset($_GET['s'])) {
			$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."&page=" . ($page+1) . "\">" . $nextlabel . "</a></li>";
			$out.= "<li><a href=\"" .$reload . "&s=".$_GET['s']."&page=" . $pmax . "\">>></a></li>";
		} else {
			$out.= "<li><a href=\"" .$reload . "&page=" . ($page+1) . "\">" . $nextlabel . "</a></li>";
			$out.= "<li><a href=\"" .$reload . "&page=" . $pmax . "\">>></a></li>";		
		}
	}
	else {
		$out.= "<li>" . $nextlabel . "</li>";
		$out.= "<li>>></li>";
	}
	
	$out.= "</ul>";
	
	return $out;

}
	if($con) { mysql_close($con);   unset($con); }

?>