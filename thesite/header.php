<?php
//echo phpinfo();
//pspell_config_create("en");
//$pspell = pspell_new("en");
/*
 * Parent dir where site is hosted. This to change all links
 */
$siteParentDir = '/thesite';



?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Shop Homepage - Start Bootstrap Template</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo $siteParentDir; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $siteParentDir; ?>/css/menu.css" rel="stylesheet">
        
        <!-- Custom CSS -->
        <link href="<?php echo $siteParentDir; ?>/css/shop-homepage.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $siteParentDir; ?>/css/slider.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>


            ul.setPaginate li.setPage {
                padding: 15px 10px;
                font-size: 14px;
            }

            ul.setPaginate {
                margin: 0px;
                padding: 0px;
                height: 100%;
                overflow: hidden;
                font: 12px 'Tahoma';
                list-style-type: none;
            }

            ul.setPaginate li.dot {
                padding: 3px 0;
            }

            ul.setPaginate li {
                float: left;
                margin: 0px;
                padding: 0px;
                margin-left: 5px;
            }

            ul.setPaginate li,
            ul.setPaginate li a {
                border: 1px solid #cccccc;
                color: #999999;
                display: inline-block;
                font: 15px/25px Arial, Helvetica, sans-serif;
                margin: 5px 3px 0 0;
                padding: 0 5px;
                text-align: center;
                text-decoration: none;
            }

            ul.setPaginate li:hover,
            ul.setPaginate li.current {
                background: none repeat scroll 0 0 #0d92e1;
                color: #ffffff;
                text-decoration: none;
            }

            ul.setPaginate li a,
            ul.setPaginate li {
                color: black;
                display: block;
                text-decoration: none;
                padding: 5px 8px;
                text-decoration: none;
            }

            ul.setPaginate li a {
                padding: 0;
                border: none;
                margin: 0;
            }

            .portfolio-item {
                margin-bottom: 25px;
            }
            .cat{

                margin-left: 20px;
            }
            .dropdown-large {
                position: static !important;
            }
            .dropdown-menu-large {
                margin-left: 16px;
                margin-right: 16px;
                padding: 20px 0px;
            }
            .dropdown-menu-large > li > ul {
                padding: 0;
                margin: 0;
            }
            .dropdown-menu-large > li > ul > li {
                list-style: none;
            }
            .dropdown-menu-large > li > ul > li > a {
                display: block;
                padding: 3px 20px;
                clear: both;
                font-weight: normal;
                line-height: 1.428571429;
                color: #333333;
                white-space: normal;
            }
            .dropdown-menu-large > li ul > li > a:hover,
            .dropdown-menu-large > li ul > li > a:focus {
                text-decoration: none;
                color: #262626;
                background-color: #f5f5f5;
            }
            .dropdown-menu-large .disabled > a,
            .dropdown-menu-large .disabled > a:hover,
            .dropdown-menu-large .disabled > a:focus {
                color: #999999;
            }
            .dropdown-menu-large .disabled > a:hover,
            .dropdown-menu-large .disabled > a:focus {
                text-decoration: none;
                background-color: transparent;
                background-image: none;
                filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
                cursor: not-allowed;
            }
            .dropdown-menu-large .dropdown-header {
                color: #428bca;
                font-size: 18px;
            }
          ul.nav li.dropdown:hover > ul.dropdown-menu {
    display: block;    
}
            @media (max-width: 768px) {
                .dropdown-menu-large {
                    margin-left: 0 ;
                    margin-right: 0 ;
                }
                .dropdown-menu-large > li {
                    margin-bottom: 30px;
                }
                .dropdown-menu-large > li:last-child {
                    margin-bottom: 0;
                }
                .dropdown-menu-large .dropdown-header {
                    padding: 3px 15px !important;
                }
                li{
                    max-width:170px; margin-bottom:8px;
                    a{ margin-left:5px; cursor:pointer;
                       &:hover{ text-decoration:none; }
                       &::before{
                           color:red;
                           content:'\00D7';
                       }
                    }
                }



                
                
            </style>
            <script src="<?php echo $siteParentDir; ?>/js/jquery1.12.js"></script>
            <script src="<?php echo $siteParentDir; ?>/js/jquery-ui.js"></script>
            <script src="<?php echo $siteParentDir; ?>/js/bootstrap.min.js"></script>
            <script type="text/javascript">

                $(document).ready(function () {
                    $("#slider-range").slider({
                        range: true,
                        min: 0,
                        max: 1000,
                        values: ['<?php echo $amount1; ?>', '<?php echo $amount2; ?>'],
                        slide: function (event, ui) {
                            $("#amount").val(ui.values[ 0 ] + "-" + ui.values[ 1 ]);
                        }
                    });
                    $("#amount").val($("#slider-range").slider("values", 0) +
                            "-" + $("#slider-range").slider("values", 1));

                });

            </script>




        </head>

        <body>

            <!-- Navigation -->

            <nav class="navbar navbar-default navbar-static">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo $siteParentDir; ?>/index.php">Giftexperience</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav" style="margin-left:160px">

                        <li >
                            <form method="get" action="<?php echo $siteParentDir; ?>/search.php">
                                <input type="text" name="term" style="margin-top: 11px;width: 633px;" value="<?php
                                if (!empty($_GET['term'])) {
                                    echo $_GET['term'];
                                }
                                ?>" />
                                <input type="submit" name="search" value="search" class="btn-primary"/>
                            </form>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="list-group">
                    <?php
                    $menu = "select * from parent_cats where parent_id=0";
                    $rsmen = mysqli_query($conn, $menu);
                    while ($rwmen = mysqli_fetch_assoc($rsmen)) {
                        ?>
                        <div class="col-sm-2 col-md-2 col-lg-2" style="border-bottom: 1px solid lightgrey;">
                            <div class="collapse navbar-collapse js-navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown dropdown-large">
                                        <a href="<?php echo $siteParentDir;?>/<?php echo $rwmen['slug'];?>/" class="dropdown-toggle" style="padding:0px !important" >
                                            <?php echo $rwmen['name']; ?></a>
                                        
                                        <?php
                                        $drp = "select * from parent_cats where parent_id=" . $rwmen['id']." order by name asc";
                                        
                                        $rsdrp = mysqli_query($conn, $drp);
                                        ?>

                                        <ul class="dropdown-menu " style="columns: 4 !important;
                                            -webkit-columns: 4 !important;
                                            -moz-columns: 4 !important;" >
                                            <li>
                                                <?php while ($rwdrp = mysqli_fetch_assoc($rsdrp)) { ?>
                                                    <a  href="<?php echo $siteParentDir; ?>/<?php echo $rwdrp['slug'] ?>/"  ><?php echo $rwdrp['name']; ?></a>
                                                <?php } ?>
                                            <li>    
                                        </ul>

                                    </li>
                                </ul>



                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </nav>
